<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Event extends CI_Controller {
	public function index(){
		$jumlah= $this->Event_model->listEvent(0)->num_rows();
		$data['title'] = 'Event - '.getTitle();
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['favicon'] = getFavicon();

		$config['base_url'] = base_url().'event/index';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 6; 	
		if ($this->uri->segment('3')!=''){
			$dari = $this->uri->segment('3');
		}else{
			$dari = 0;
		}

		if (is_numeric($dari)) {
			$data['event'] = $this->Event_model->getList($dari, $config['per_page']);
		}else{
			redirect('main');
		}
		$this->pagination->initialize($config);

		$this->template->load('layouts/Template','event/index',$data);
		$this->load->view('main/rss');
	}

	public function detail(){
		$ids = $this->uri->segment(3);
		$dat = $this->Event_model->getDetail($this->db->escape_str($ids));
	    $row = $dat->row();
	    $total = $dat->num_rows();
        if ($total == 0){
        	redirect('main');
        }
		$data['title'] = getCetak($row->judul);
		$data['description'] = getCetak($row->keterangan);
		$data['keywords'] = getKeywords();
		$data['favicon'] = getFavicon();

		$data['detail'] = $this->Event_model->getDetailEvent($row->id_event)->row_array();
		$data['eventLainnya'] = $this->Event_model->eventLainnya($row->id_event);
		$data['terkaitGallery'] = $this->Event_model->terkaitGallery($row->id_event);
		$this->Event_model->getReads($ids);
		$this->template->load('layouts/Template','event/_detail',$data);
	}
}
