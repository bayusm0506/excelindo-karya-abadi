<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Event extends CI_Controller {
	public function index(){
		if (isset($_POST['kata'])) {
			$keyword = strip_tags($this->input->post('kata'));
			$data['title'] = 'Hasil Pencarian dengan keyword : '.$keyword;
			$data['description'] = getDescription();
			$data['keywords'] = getKeywords();
			$data['favicon'] = getFavicon();
			$data['utama'] = $this->ListBerita_model->getCariData(0,5,$keyword);
		}else{
			$jumlah= $this->ListBerita_model->getCountBeritaUtama()->num_rows();
			$data['title'] = 'Event - '.getTitle();
			$data['description'] = getDescription();
			$data['keywords'] = getKeywords();
			$data['favicon'] = getFavicon();

			$config['base_url'] = base_url().'blog/index';
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 5; 	
			if ($this->uri->segment('3')!=''){
				$dari = $this->uri->segment('3');
			}else{
				$dari = 0;
			}

			if (is_numeric($dari)) {
				$data['utama'] = $this->ListBerita_model->getUtama($dari, $config['per_page']);
			}else{
				redirect('main');
			}
			$this->pagination->initialize($config);
		}

		$this->template->load('layouts/Template','event/index',$data);
		$this->load->view('main/rss');
	}

	public function detail(){
		$ids = $this->uri->segment(3);
		$dat = $this->ListBerita_model->getDetail($this->db->escape_str($ids));
	    $row = $dat->row();
	    $total = $dat->num_rows();
        if ($total == 0){
        	redirect('main');
        }
		$data['title'] = getCetak($row->judul);
		$data['description'] = getCetak($row->isi_berita);
		$data['keywords'] = getKeywords();
		$data['record'] = $this->ListBerita_model->getBeritaDetail($ids)->row_array();
		$data['gallery'] = $this->BlogGallery_model->getBlogGallery($row->id_berita)->result_array();
		$data['jumGallery'] = $this->BlogGallery_model->getBlogGallery($row->id_berita)->num_rows();
		$data['favicon'] = getFavicon();
		$this->ListBerita_model->getBeritaDibacaUpdate($ids);
		$this->template->load('layouts/Template','blog/_berita_detail',$data);
	}

	public function kategori(){
		$ids = $this->uri->segment(3);
		$jumlah= $this->ListBerita_model->getGroupKategoriDetail($ids)->num_rows();
		$getById = $this->ListBerita_model->getGroupKategoriId($ids);
		$row = $getById->row();

		$data['title'] = 'Blog Kategori '.getCetak($row->nama_kategori);
		$data['description'] = 'Blog Kategori '.getCetak($row->nama_kategori);
		$data['keywords'] = getKeywords();
		$data['favicon'] = getFavicon();
		$data['record'] = $this->ListBerita_model->getGroupKategoriId($ids)->row_array();
		$config['base_url'] = base_url().'blog/kategori/'.$ids.'/index';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 5; 	
		if ($this->uri->segment('5')!=''){
			$dari = $this->uri->segment('5');
		}else{
			$dari = 0;
		}

		if (is_numeric($dari)) {
			$data['utama'] = $this->ListBerita_model->getUtamaKategori($ids, $dari, $config['per_page']);
		}else{
			redirect('main');
		}
		$this->pagination->initialize($config);

		$this->template->load('layouts/Template','blog/_kategori',$data);
		$this->load->view('main/rss');
	}
}
