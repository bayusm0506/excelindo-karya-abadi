<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Main extends CI_Controller {
	public function index(){
		$data['title'] = getTitle();
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['favicon'] = getFavicon();

		$data['lang'] = $this->session->userdata('language');
		$data['captcha'] = $this->recaptcha->getWidget();

		$this->template->load('layouts/Template','main/index',$data);
		$this->load->view('main/rss');
	}

	public function save(){
		$data = array(
            'nama'=>$this->input->post('nama'),
			'email'=>$this->input->post('email'),
			'subjek'=>$this->input->post('subjek'),
			'pesan'=>$this->input->post('isi_pesan'),
			'dibaca'=>'Y',
			'tanggal'=>date('Y-m-d'),
			'jam'=>date('H:i:s')
		);
		$recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);

        if (!isset($response['success']) || $response['success'] <> true) {
        	echo json_encode(array("status" => 'error', 'msg'=>"Chaptca harus diisi"));
        }else{
        	$nama           = $this->input->post('nama');
	        $email           = $this->input->post('email');
	        $subject         = $this->input->post('subjek');
	        $message         = $this->input->post('isi_pesan');

	        $this->email->from($email, $nama);
	        $this->email->to(getEmail());
	        $this->email->cc('');
	        $this->email->bcc('');

	        $this->email->subject($subject);
	        $this->email->message($message);
	        $this->email->set_mailtype("html");
	        $this->email->send();
	        
	        $config['protocol'] = 'sendmail';
	        $config['mailpath'] = '/usr/sbin/sendmail';
	        $config['charset'] = 'utf-8';
	        $config['wordwrap'] = TRUE;
	        $config['mailtype'] = 'html';
	        $this->email->initialize($config);
	        
        	$insert = $this->PesanMasuk_model->save($data);
			echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Dikirim, Kami Akan Segera Merespon Secepatnya"));
        }
	}
}
