<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Product extends CI_Controller {
	public function index(){
		if (isset($_GET['frame'])) {
			$produk = strip_tags($this->input->get('nama_produk', TRUE));
			$hasil = "";
			if ($produk != '') {
				$hasil .= "Produk : ".$produk;
			}
			$data['title'] = 'Hasil Pencarian dengan keyword '.$hasil;
			$data['description'] = getDescription();
			$data['keywords'] = getKeywords();
			$data['favicon'] = getFavicon();

			$jumlah= $this->ProductGallery_model->count_all_product($produk)->num_rows();
			//echo $jumlah;
			$config['page_query_string'] = TRUE;
			$config['base_url'] = base_url().'product/index/?produk='.$produk.'&cari=';
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 5; 
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';	
			$config['prev_link'] = 'Prev';
			$config['next_link'] = 'Next';

			$fetch_data = $this->input->get('per_page', TRUE);
			if ($fetch_data != ''){
				$dari = $fetch_data;
			}else{
				$dari = 0;
			}

			if (is_numeric($dari)) {
				$data['produk'] = $this->ProductGallery_model->getCariData($dari, $config['per_page'],$produk);
			}else{
				redirect('main');
			}
			$this->pagination->initialize($config);
		}
		else{
			$jumlah= $this->ProductGallery_model->getProduct()->num_rows();
			$data['title'] = 'Produk';
			$data['description'] = getDescription();
			$data['keywords'] = getKeywords();
			$data['favicon'] = getFavicon();

			$config['base_url'] = base_url().'product/index';
			$config['total_rows'] = $jumlah;
			$config['per_page'] = 5; 
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';	
			$config['prev_link'] = 'Prev';
			$config['next_link'] = 'Next';
			if ($this->uri->segment('3')!=''){
				$dari = $this->uri->segment('3');
			}else{
				$dari = 0;
			}

			if (is_numeric($dari)) {
				$data['produk'] = $this->ProductGallery_model->getProductList($dari, $config['per_page']);
			}else{
				redirect('main');
			}
			$this->pagination->initialize($config);
		}

		$this->template->load('layouts/Template','product/index',$data);
		$this->load->view('main/rss');
	}

	public function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->ProductGallery_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->jdl_pg;
                echo json_encode($arr_result);
            }
        }
    }

    public function category(){
    	$seo = $this->uri->segment('3');
    	$dat = $this->MenuGroup_model->menu_cek_($seo);
	    $row = $dat->row();
	    $total = $dat->num_rows();
        if ($total == 0){
        	redirect('main');
        }

    	$jumlah= $this->ProductGallery_model->getProductCount($row->id_group_menu)->num_rows();
		$data['title'] = getCetak($row->nama_group_menu);
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['favicon'] = getFavicon();

		$config['base_url'] = base_url().'product/category/'.$seo.'/';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 5; 
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';	
		$config['prev_link'] = 'Prev';
		$config['next_link'] = 'Next';
		if ($this->uri->segment('4')!=''){
			$dari = $this->uri->segment('4');
		}else{
			$dari = 0;
		}

		if (is_numeric($dari)) {
			$data['produk'] = $this->ProductGallery_model->getCatProductList($dari, $config['per_page'], $row->id_group_menu);
		}else{
			redirect('main');
		}
		$this->pagination->initialize($config);

		$this->template->load('layouts/Template','product/_category',$data);
		$this->load->view('main/rss');
    }

	public function detail(){
		$id_produk = $this->uri->segment(3);
		$row = $this->ProductGallery_model->get_by_id($id_produk);
		$cek = $this->ProductGallery_model->getDetail($id_produk)->num_rows();
		if (count($cek == 0)) {
			redirect('main');
		}
		$data['title'] = getCetak($row->jdl_pg);
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['record'] = $this->ProductGallery_model->getDetail($id_produk)->row_array();
		$data['favicon'] = getFavicon();

		$this->template->load('layouts/Template','product/_detail',$data);
		$this->load->view('main/rss');
	}
}
