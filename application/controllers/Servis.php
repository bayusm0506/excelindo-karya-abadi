<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Servis extends CI_Controller {
	public function index(){
		$data['title'] = 'Layanan';
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['favicon'] = getFavicon();
		$data['servis'] = $this->Servis_model->getServis();

		$this->template->load('layouts/Template','servis/index',$data);
		$this->load->view('main/rss');
	}
}
