<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Team extends CI_Controller {
	public function index(){
		$data['title'] = 'Our Team - '.getTitle();
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['favicon'] = getFavicon();
		$data['team'] = $this->Team_model->getData();
		$this->template->load('layouts/Template','team/index',$data);
		$this->load->view('main/rss');
	}
}
