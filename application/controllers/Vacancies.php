<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Vacancies extends CI_Controller {
	public function index(){
		$jumlah= $this->Position_model->getCountPosition()->num_rows();
		$data['title'] = 'Vacancies - '.getTitle();
		$data['description'] = getDescription();
		$data['keywords'] = getKeywords();
		$data['favicon'] = getFavicon();

		$config['base_url'] = base_url().'vacancies/index';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 4; 	
		if ($this->uri->segment('3')!=''){
			$dari = $this->uri->segment('3');
		}else{
			$dari = 0;
		}

		if (is_numeric($dari)) {
			$data['list'] = $this->Position_model->getList($dari, $config['per_page']);
		}else{
			redirect('main');
		}
		$this->pagination->initialize($config);

		$data['position'] = $this->Position_model->getCountPosition()->result_array();
		$this->template->load('layouts/Template','vacancies/index',$data);
		$this->load->view('main/rss');
	}

	public function ajax_edit($id)
	{
		$data = $this->Position_model->get_by_id($id);
		echo json_encode($data);
	}

	public function rules(){
		return array(
			array('field'=>'name','label'=>'Nama','rules'=>'required'),
       		array('field'=>'phone','label'=>'Nomor HP','rules'=>'required'),
       		array('field'=>'email','label'=>'Email','rules'=>'required'),
       		array('field'=>'position','label'=>'Position','rules'=>'required')
		);
	}

	public function save()
   	{
       	$rules = self::rules();
       	$this->form_validation->set_rules($rules);
       	$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong');

        if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
        }else{
        	$data = array(
	          	'name'=>$this->input->post('name'),
	          	'phone'=>$this->input->post('phone'),
	          	'email'=>$this->input->post('email'),
	          	'position'=>$this->input->post('position'),
	          	'verified'=>'N',
	          	'tgl_posting'=>date('Y-m-d'),
	          	'jam'=>date('H:i:s'),
	          	'hari'=>hari_ini(date('w'))
			);

			if(!empty($_FILES['attach1']['name']))
			{
				$upload = $this->_do_upload_attach1();
				$data['attachment1'] = $upload;
			}

			if(!empty($_FILES['attach2']['name']))
			{
				$upload = $this->_do_upload_attach2();
				$data['attachment2'] = $upload;
			}

			if(!empty($_FILES['attach3']['name']))
			{
				$upload = $this->_do_upload_attach3();
				$data['attachment3'] = $upload;
			}

			if(!empty($_FILES['attach4']['name']))
			{
				$upload = $this->_do_upload_attach4();
				$data['attachment4'] = $upload;
			}

        	$this->Vacancies_model->save($data);
            $response = ["status"=>'info','msg'=>'Data Berhasil di Kirim, Kami akan menghubungi Anda jika kualifikasi memenuhi kriteria.'];
            echo json_encode($response);
        }
    }

	private function _do_upload_attach1()
	{
		$config['upload_path']          = './asset/file_vacancies/';
        $config['allowed_types']        = 'gif|jpg|png|zip|rar';
        $config['max_size']             = 3000; //set max size allowed in Megabyte
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('attach1')) //upload and validate
        {
			$errors = "File tidak diperbolehkan Upload, File yang diperbolehkan Upload hanya format ekstensi gif|jpg|png|zip|rar dengan ukuran Max:3Mb";
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
            exit();
		}
		return $this->upload->data('file_name');
	}

	private function _do_upload_attach2()
	{
		$config['upload_path']          = './asset/file_vacancies/';
        $config['allowed_types']        = 'gif|jpg|png|zip|rar';
        $config['max_size']             = 3000; //set max size allowed in Megabyte
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('attach2')) //upload and validate
        {
			$errors = "File tidak diperbolehkan Upload, File yang diperbolehkan Upload hanya format ekstensi gif|jpg|png|zip|rar dengan ukuran Max:3Mb";
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
            exit();
		}
		return $this->upload->data('file_name');
	}

	private function _do_upload_attach3()
	{
		$config['upload_path']          = './asset/file_vacancies/';
        $config['allowed_types']        = 'gif|jpg|png|zip|rar';
        $config['max_size']             = 3000; //set max size allowed in Megabyte
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('attach3')) //upload and validate
        {
			$errors = "File tidak diperbolehkan Upload, File yang diperbolehkan Upload hanya format ekstensi gif|jpg|png|zip|rar dengan ukuran Max:3Mb";
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
            exit();
		}
		return $this->upload->data('file_name');
	}

	private function _do_upload_attach4()
	{
		$config['upload_path']          = './asset/file_vacancies/';
        $config['allowed_types']        = 'gif|jpg|png|zip|rar';
        $config['max_size']             = 3000; //set max size allowed in Megabyte
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('attach4')) //upload and validate
        {
			$errors = "File tidak diperbolehkan Upload, File yang diperbolehkan Upload hanya format ekstensi gif|jpg|png|zip|rar dengan ukuran Max:3Mb";
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
            exit();
		}
		return $this->upload->data('file_name');
	}
}
