<?php

defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Video extends CI_Controller {

	public function index(){

		$data['title'] = 'Videos';

		$data['description'] = getDescription();

		$data['keywords'] = getKeywords();

		$data['favicon'] = getFavicon();

		$data['video'] = $this->Videos_model->getVideos();



		$this->template->load('layouts/Template','video/index',$data);

		$this->load->view('main/rss');

	}

}

