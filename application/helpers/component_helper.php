<?php
	function getKategoriBerita(){
    	$CI = & get_instance();
        $CI->load->model(array('Dashboard_model'));
        return $kategori = $CI->Dashboard_model->getKategori();
    }
    function getDataHome(){
        $ci = & get_instance();
        $ci->load->model(array('Model_berita','Model_halaman', 'Model_agenda', 'Model_users'));
        $news = $ci->Model_berita->info_news();
        $pages = $ci->Model_halaman->info_pages();
        $agenda = $ci->Model_agenda->info_agenda();
        $users = $ci->Model_users->info_users();
        return $data = array(
            'news'=>$news,
            'pages'=>$pages,
            'agenda'=>$agenda,
            'users'=>$users
        );
    }

    function getKategoriMenuUtama(){
        $CI = & get_instance();
        $CI->load->model(array('MenuWebsite_model'));
        return $kategori = $CI->MenuWebsite_model->menu_utama();
    }

    function getTitle(){
        $CI = & get_instance();
        $CI->load->model(array('MenuUtama_model'));
        $title = $CI->MenuUtama_model->identitas()->row_array();
        return $title['nama_website'];
    }

    function getDescription(){
        $CI = & get_instance();
        $CI->load->model(array('MenuUtama_model'));
        $title = $CI->MenuUtama_model->identitas()->row_array();
        return $title['meta_deskripsi'];
    }

    function getKeywords(){
        $CI = & get_instance();
        $CI->load->model(array('MenuUtama_model'));
        $title = $CI->MenuUtama_model->identitas()->row_array();
        return $title['meta_keyword'];
    }

    function getFavicon(){
        $CI = & get_instance();
        $CI->load->model(array('MenuUtama_model'));
        $title = $CI->MenuUtama_model->identitas()->row_array();
        return $title['favicon'];
    }

    function getEmail(){
        $CI = & get_instance();
        $CI->load->model(array('MenuUtama_model'));
        $title = $CI->MenuUtama_model->identitas()->row_array();
        return $title['email'];
    }

    function getCetak($str){
        return strip_tags(htmlentities($str, ENT_QUOTES, 'UTF-8'));
    }

    function getChoose(){
        $CI = & get_instance();
        $CI->load->model(array('WhyChoose_model'));
        $choose = $CI->WhyChoose_model->data();
        return $choose;
    }