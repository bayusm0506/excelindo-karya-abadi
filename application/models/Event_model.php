<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Event_model extends CI_Model
{
  var $table = 'event';
  var $column_order = array(null,'judul','tgl_event','status','images'); //set column field database for datatable orderable
  var $column_search = array('judul'); //set column field database for datatable searchable just firstname , lastname , address are searchable
  var $order = array('id_event' => 'ASC'); // default order 

  private function _get_datatables_query()
  {
    //add custom filter here
        if($this->input->post('judul'))
        {
            $this->db->like('judul', $this->input->post('judul'));
        }
        if($this->input->post('tgl_event'))
        {
            $this->db->where('tgl_event', $this->input->post('tgl_event'));
        }
        if($this->input->post('status'))
        {
            $this->db->where('status', $this->input->post('status'));
        }

    $this->db->from($this->table);
    $i = 0;
  
    foreach ($this->column_search as $item) // loop column 
    {
      if(isset($_POST['search']['value'])) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  public function delete($id){
    $this->db->where('id_event', $id);
    $result=$this->db->delete($this->table);
    return $result;
  }

  public function save($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function autocomplete($judul){
      $this->db->like('judul', $judul , 'both');
      $this->db->order_by('judul', 'ASC');
      $this->db->limit(10);
      return $this->db->get($this->table)->result();
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_event',$id);
    $query = $this->db->get();
    return $query->row();
  }

  // Function is use Controller EventGallery
  public function listEvent($id){
    if ($id > 0) {
      $this->db->where('id_event', $id);
    }else{
      $this->db->where('status', 'Y');
    }
    return $this->db->get($this->table);
  }

  //Function is use controller Event Frontend
  public function getList($start,$limit){
    $sql = "SELECT * FROM $this->table";
    if(get_cookie('lang_is') === 'en'){$sql .= " WHERE bahasa='ENG'";}else{$sql .= " WHERE bahasa='IDN'";}
    $sql .= " ORDER BY id_event DESC LIMIT $start, $limit";
    return $this->db->query($sql);
  }

  //Function is use controller Event Frontend
  public function getDetail($jdl_seo){
      $this->db->from($this->table);
      $this->db->where('jdl_seo', $jdl_seo);
      return $this->db->get();
  }  

  //Function is use controller Event Frontend
  public function getReads($id){
      return $this->db->query("UPDATE $this->table SET dibaca=dibaca+1 
                    WHERE id_event='".$this->db->escape_str($id)."' OR jdl_seo='".$this->db->escape_str($id)."'");
  }

  public function getDetailEvent($id){
    return $this->db->query("SELECT * FROM $this->table A
                              LEFT JOIN event_gallery B
                                ON A.id_event = B.id_event 
                                  WHERE A.status = 'Y' AND A.id_event='$id'");
  }

  public function eventLainnya($id){
    return $this->db->query("SELECT * FROM $this->table A
                              LEFT JOIN event_gallery B
                                ON A.id_event = B.id_event 
                                  WHERE A.status = 'Y' AND A.id_event <> $id GROUP BY A.id_event ORDER BY A.id_event LIMIT 3");
  }

  public function terkaitGallery($id){
    return $this->db->query("SELECT * FROM event_gallery WHERE id_event='$id'");
  }
}