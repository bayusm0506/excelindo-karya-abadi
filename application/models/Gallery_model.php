<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Gallery_model extends CI_Model
{
  var $table = 'gallery';
  var $column_order = array(null,'gbr_gallery','jdl_gallery','id_gallery'); //set column field database for datatable orderable
  var $column_search = array('gbr_gallery','jdl_gallery'); //set column field database for datatable searchable just firstname , lastname , address are searchable
  var $order = array('id_gallery' => 'ASC'); // default order 

  private function _get_datatables_query()
  {
    //add custom filter here
    if($this->input->post('jdl_gallery'))
    {
        $this->db->like('jdl_gallery', $this->input->post('jdl_gallery'));
    }

    if($this->input->post('id_album'))
    {
        $this->db->like('id_album', $this->input->post('id_album'));
    }

    $this->db->from($this->table);
    $i = 0;
  
    foreach ($this->column_search as $item) // loop column 
    {
      if(isset($_POST['search']['value'])) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables($id)
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    if ($id != 'admin') {
      $this->db->where('username', $id);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  public function delete($id){
    $this->db->where('id_gallery', $id);
    return $this->db->delete($this->table);
  }

  public function save($data){
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update($where, $data){
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function autocomplete($jdl_gallery){
    $this->db->like('jdl_gallery', $jdl_gallery , 'both');
    $this->db->order_by('jdl_gallery', 'ASC');
    $this->db->limit(10);
    return $this->db->get($this->table)->result();
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_gallery',$id);
    $query = $this->db->get();
    return $query->row();
  }

  //Function is use views frontend
  public function getGallery()
  {
    $this->db->from('gallery as A');
    $this->db->join('album as B','A.id_album = B.id_album','LEFT');
    $this->db->where('B.aktif', 'Y');
    if(get_cookie('lang_is') === 'en'){$this->db->where('A.bahasa','ENG');}else{$this->db->where('A.bahasa','IDN');}
    $this->db->order_by('A.id_gallery', 'DESC');
    $this->db->limit(4);
    return $this->db->get();
  }

  //Function is use controller portfolio
  public function getGalleryAlbum($id){
    if ($id > 0) {
      $data = " AND B.id_album = $id";
    }else{
      $data = "";
    }
    return $this->db->query("
      SELECT * FROM $this->table A 
      LEFT JOIN album B
        ON A.id_album = B.id_album
      LEFT JOIN users C
        ON A.username = C.username
      WHERE B.aktif = 'Y'
      $data
    ");
  }

  //Function is use controller portfolio
  public function getDetail($gallery_seo){
      $this->db->from($this->table);
      $this->db->where('gallery_seo', $gallery_seo);
      return $this->db->get();
  }

  //Function is use controller portfolio
  public function getPortfolioDetail($id){
        return $this->db->query("SELECT a.jdl_gallery, a.gbr_gallery, a.gbr_gallery2, a.gbr_gallery3, a.keterangan FROM $this->table a 
                    LEFT JOIN users b ON a.username=b.username 
                        LEFT JOIN album c ON a.id_album=c.id_album 
                          WHERE (a.id_gallery='".$this->db->escape_str($id)."' OR a.gallery_seo='".$this->db->escape_str($id)."')");
  }
}