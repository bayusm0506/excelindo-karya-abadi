<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class MenuGroup_model extends CI_Model
{
	var $table = 'group_menu';
	var $column_order = array('id_group_menu','nama_group_menu','status',null); //set column field database for datatable orderable
	var $column_search = array('id_group_menu','nama_group_menu','status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = array('id_group_menu' => 'ASC'); // default order 

	private function _get_datatables_query()
	{
		//add custom filter here
        if($this->input->post('nama_group_menu'))
        {
            $this->db->like('nama_group_menu', $this->input->post('nama_group_menu'));
        }
        if($this->input->post('status'))
        {
            $this->db->where('status', $this->input->post('status'));
        }

		$this->db->from($this->table);
		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if(isset($_POST['search']['value'])) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function rules(){
		return array(
			array('field'=>'nama_group_menu','label'=>'Nama Group Menu','rules'=>'required'),
			array('field'=>'status','label'=>'Status Group Menu','rules'=>'required')
		);
	}
	/* For MenuGroup_list */
	function menuGroup(){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->order_by('id_group_menu', 'ASC');
		return $this->db->get();
    }

    public function menu_cek($id){
    	$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('id_group_menu', $id);
		return $this->db->get();
    }

    public function menu_cek_($seo){
    	$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('seo', $seo);
		return $this->db->get();
    }
    /* End For MenuGroup_list */
    
    public function delete($id){
		$this->db->where('id_group_menu', $id);
		$result=$this->db->delete($this->table);
		return $result;
    }

    public function save($data){
        $this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

    public function autocomplete($nama_group_menu){
        $this->db->like('nama_group_menu', $nama_group_menu , 'both');
        $this->db->order_by('nama_group_menu', 'ASC');
        $this->db->limit(10);
        return $this->db->get($this->table)->result();
    }

    public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id_group_menu',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function getMenu()
  	{
	    $this->db->from($this->table);
	    $this->db->where('status', 'Y');
	    return $this->db->get();
  	}

  	//Function is use controller ProductGallery
	public function listGroup($id){
		$this->db->order_by('id_group_menu', 'DESC');
	    if ($id > 0) {
	      $this->db->limit($id);
	    }
		return $this->db->get($this->table);
	}
}