<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Playlist_model extends CI_Model
{
  var $table = 'playlist';
  var $column_order = array(null,'gbr_playlist','jdl_playlist','aktif'); //set column field database for datatable orderable
  var $column_search = array('jdl_playlist'); //set column field database for datatable searchable just firstname , lastname , address are searchable
  var $order = array('id_playlist' => 'ASC'); // default order 

  private function _get_datatables_query()
  {
    //add custom filter here
    if($this->input->post('jdl_playlist'))
    {
        $this->db->like('jdl_playlist', $this->input->post('jdl_playlist'));
    }

    if($this->input->post('aktif'))
    {
        $this->db->where('aktif', $this->input->post('aktif'));
    }

    $this->db->from($this->table);
    $i = 0;
  
    foreach ($this->column_search as $item) // loop column 
    {
      if(isset($_POST['search']['value'])) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables($id)
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    if ($id != 'admin') {
      $this->db->where('username', $id);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  public function delete($id){
    $this->db->where('id_playlist', $id);
    return $this->db->delete($this->table);
  }

  public function save($data){
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update($where, $data){
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function autocomplete($jdl_playlist){
    $this->db->like('jdl_playlist', $jdl_playlist , 'both');
    $this->db->order_by('jdl_playlist', 'ASC');
    $this->db->limit(10);
    return $this->db->get($this->table)->result();
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_playlist',$id);
    $query = $this->db->get();
    return $query->row();
  }

  //Function is use controller video
  public function getPlaylist(){
    $this->db->order_by('id_playlist', 'DESC');
    return $this->db->get($this->table);
  }

  //Function is use controller video
  public function menu_cek($id){
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->where('id_playlist', $id);
    return $this->db->get();
    }
}