<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Position_model extends CI_Model
{
  var $table = 'job_position';
  var $column_order = array(null,'position','tgl_deadline'); //set column field database for datatable orderable
  var $column_search = array('position'); //set column field database for datatable searchable just firstname , lastname , address are searchable
  var $order = array('id_position' => 'DESC'); // default order 

  private function _get_datatables_query()
  {
    //add custom filter here
        if($this->input->post('tgl_deadline'))
        {
            $this->db->where('tgl_deadline', $this->input->post('tgl_deadline'));
        }

    $this->db->from($this->table);
    $i = 0;
  
    foreach ($this->column_search as $item) // loop column 
    {
      if(isset($_POST['search']['value'])) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  public function delete($id){
		$this->db->where('id_position', $id);
		$result=$this->db->delete($this->table);
		return $result;
  }

  public function save($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_position',$id);
    $query = $this->db->get();

    return $query->row();
  }

  //Function use controller Vacancies Modules
  public function menu_cek($id){
    $this->db->select('*');
    $this->db->from($this->table);
    if ($id != '') {
      $this->db->where('id_position', $id);
    }
    return $this->db->get();
  }

  //Function is use controller Vacancies Frontend
  public function getCountPosition(){
    return $this->db->get($this->table);
  }

  //Function is use controller Vacancies Frontend
  public function getList($start,$limit){
    return $this->db->query("SELECT * FROM $this->table ORDER BY id_position DESC LIMIT $start, $limit");
  }
}