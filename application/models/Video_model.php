<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Video_model extends CI_Model
{
  var $table = 'video';
  var $column_order = array(null,'id_playlist','tanggal','jdl_video'); //set column field database for datatable orderable
  var $column_search = array('jdl_video'); //set column field database for datatable searchable just firstname , lastname , address are searchable
  var $order = array('id_playlist' => 'DESC'); // default order 

  private function _get_datatables_query()
  {
    //add custom filter here
        if($this->input->post('jdl_video'))
        {
            $this->db->like('jdl_video', $this->input->post('jdl_video'));
        }
        if($this->input->post('id_playlist'))
        {
            $this->db->like('id_playlist', $this->input->post('id_playlist'));
        }
        if($this->input->post('tanggal'))
        {
            $this->db->where('tanggal', $this->input->post('tanggal'));
        }

    $this->db->from($this->table);
    $i = 0;
  
    foreach ($this->column_search as $item) // loop column 
    {
      if(isset($_POST['search']['value'])) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables($user)
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    if ($user != 'admin') {
      $this->db->where('username', $user);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

	public function rules(){
		return array(
			array('field'=>'jdl_video','label'=>'Jdl_video Video','rules'=>'required'),
      array('field'=>'id_playlist','label'=>'Playlist','rules'=>'required')
		);
	}

  public function delete($id){
    $this->db->where('id_video', $id);
    $result=$this->db->delete($this->table);
    return $result;
  }

  public function save($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function autocomplete($jdl_video){
      $this->db->like('jdl_video', $jdl_video , 'both');
      $this->db->order_by('jdl_video', 'ASC');
      $this->db->limit(10);
      return $this->db->get($this->table)->result();
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_video',$id);
    $query = $this->db->get();
    return $query->row();
  }

  public function get_by_id_edit($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_video',$id);
    return $this->db->get();
  }
  
}