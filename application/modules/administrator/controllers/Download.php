<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Download extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->user_id);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Download */
    public function index(){
		$this->data['title'] = 'File Download';
		self::actionDashboard();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('download' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_judul(){
        if (isset($_GET['term'])) {
            $result = $this->Download_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->judul;
                echo json_encode($arr_result);
            }
        }
    }

	public function download_ajax_list()
	{
		$list = $this->Download_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $download) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $download->judul;
			if($download->nama_file)
				$row[] = '<a href="'.base_url('./asset/files/').$download->nama_file.'" target="_blank">Download</a>';
			else
				$row[] = '(No File)';

			$row[] = $download->hits." Kali";
			$row[] = tgl_indo($download->tgl_posting);

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_download('."'".$download->id_download."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_download('."'".$download->id_download."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Download_model->count_all(),
						"recordsFiltered" => $this->Download_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function export_pdfMW(){
		$data = array(
			'title' => 'Data Gallery',
			//'list' => $this->Gallery_model->getPerdana()
		);
	    $filename = "Bayu Setra Maulana";
	    $this->load->library('pdf');

	    $this->pdf->setPaper('A4', 'potrait');
	    $this->pdf->filename = $filename.".pdf";
		$this->pdf->load_view('menu_utama' . DIRECTORY_SEPARATOR . '_exportPdfMW', $data);

	}

	public function save()
	{
		$this->_validate();

		$data = array(
            'judul'=>$this->db->escape_str($this->input->post('judul')),
            'tgl_posting'=>date('Y-m-d'),
            'hits'=>'0'
		);

		if(!empty($_FILES['file_download']['name']))
		{
			$upload = $this->_do_upload();
			$data['nama_file'] = $upload;
		}

		$insert = $this->Download_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	public function ajax_edit($id)
	{
		$data = $this->Download_model->get_by_id($id);
		echo json_encode($data);
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/files/';
        $config['allowed_types']        = 'gif|jpg|png|zip|rar|pdf|doc|docx|ppt|pptx|xls|xlsx|txt';
        $config['max_size']             = 25000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('file_download')) //upload and validate
        {
            $data['inputerror'][] = 'file_download';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('judul') == '')
		{
			$data['inputerror'][] = 'judul';
			$data['error_string'][] = 'Judul Download Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function edit(){
        $this->_validate();

		$data = array(
			'judul'=>$this->db->escape_str($this->input->post('judul')),
            'tgl_posting'=>date('Y-m-d')
		);

		if($this->input->post('remove_file_download')) // if remove file_download checked
		{
			if(file_exists('./asset/files/'.$this->input->post('remove_file_download')) && $this->input->post('remove_file_download'))
				unlink('./asset/files/'.$this->input->post('remove_file_download'));
			$data['nama_file'] = '';
		}

		if(!empty($_FILES['file_download']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$download = $this->Download_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/files/'.$download->nama_file) && $download->nama_file)
				unlink('./asset/files/'.$download->nama_file);

			$data['nama_file'] = $upload;
		}


		$this->Download_model->update(array('id_download' => $this->input->post('id')), $data);
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function delete(){
		if($_POST['empid']) {
			$download = $this->Download_model->get_by_id($_POST['empid']);
				unlink('./asset/files/'.$download->nama_file);
			$resultset = $this->Download_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Download */

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
