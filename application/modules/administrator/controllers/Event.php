<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Event extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

	/* Funtion Action Event */
	public function index(){
		$this->data['title'] = 'Event';
		self::actionDashboard();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('event' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->Event_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->judul;
                echo json_encode($arr_result);
            }
        }
    }

	public function ajax_list()
	{
		$list = $this->Event_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $event) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $event->judul;
			$row[] = tgl_indo($event->tgl_event);
			$row[] = $event->jam_range;

			if ($event->status == 'Y') {
				$row[] = "Aktif";
			}else{
				$row[] = "Tidak Aktif";
			}

			if($event->images)
				$row[] = '<a href="'.base_url('./asset/foto_event/').$event->images.'" target="_blank"><img width="40" height="40" class="img-circle" src="'.base_url('./asset/foto_event/').$event->images.'"></a>';
			else
				$row[] = '(No photo)';

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_ev('."'".$event->id_event."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_ev('."'".$event->id_event."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Event_model->count_all(),
						"recordsFiltered" => $this->Event_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function save(){
        $this->_validate();
       	$data = array(
          	'judul'=>$this->db->escape_str($this->input->post('judul')),
          	'jdl_seo'=>seo_title($this->input->post('judul')),
          	'keterangan'=>$this->input->post('keterangan'),
          	'tgl_event'=>$this->input->post('tgl_event'),
          	'jam_range'=>$this->input->post('jam_range'),
	        'tgl_posting'=>date('Y-m-d'),
	        'hari'=>hari_ini(date('w')),
	        'jam'=>date('H:i:s'),
	        'username'=>$this->session->username,
	        'dibaca'=>'0',
	        'status'=>$this->input->post('status')
		);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			$data['images'] = $upload;
		}

		$insert = $this->Event_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/foto_event/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('judul') == '')
		{
			$data['inputerror'][] = 'judul';
			$data['error_string'][] = 'Judul Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		if($this->input->post('tgl_event') == '')
		{
			$data['inputerror'][] = 'tgl_event';
			$data['error_string'][] = 'Tanggal Event Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_edit($id)
	{
		$data = $this->Event_model->get_by_id($id);
		echo json_encode($data);
	}

	public function edit(){
        $this->_validate();

		$data = array(
          	'judul'=>$this->db->escape_str($this->input->post('judul')),
          	'jdl_seo'=>seo_title($this->input->post('judul')),
          	'keterangan'=>$this->input->post('keterangan'),
          	'tgl_event'=>$this->input->post('tgl_event'),
          	'jam_range'=>$this->input->post('jam_range'),
	        'tgl_posting'=>date('Y-m-d'),
	        'hari'=>hari_ini(date('w')),
	        'jam'=>date('H:i:s'),
	        'username'=>$this->session->username,
	        'status'=>$this->input->post('status')
		);

		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('./asset/foto_event/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('./asset/foto_event/'.$this->input->post('remove_photo'));
			$data['images'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$album = $this->Event_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/foto_event/'.$album->images) && $album->images)
				unlink('./asset/foto_event/'.$album->images);

			$data['images'] = $upload;
		}


		$this->Event_model->update(array('id_event' => $this->input->post('id')), $data);
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function delete(){
		if($_POST['empid']) {
			$event = $this->Event_model->get_by_id($_POST['empid']);
			if(file_exists('./asset/foto_event/'.$event->images) && $event->images)
				unlink('./asset/foto_event/'.$event->images);
			$resultset = $this->Event_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Funtion Action Event */

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}