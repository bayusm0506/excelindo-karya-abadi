<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Identitas extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->user_id);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Identitas */
    public function index(){
		$this->data['title'] = 'Identitas';
		self::actionDashboard();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('identitas' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function ajax_list()
	{
		$list = $this->Identitas_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $ident) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $ident->nama_website;
			$row[] = $ident->address;
			$row[] = $ident->email;
			$row[] = $ident->open;
			$row[] = $ident->no_telp;
			$row[] = $ident->whatsapp;
			if($ident->favicon)
				$row[] = '<a href="'.base_url('./asset/images/').$ident->favicon.'" target="_blank"><img width="40" height="40" class="img-circle" src="'.base_url('./asset/images/').$ident->favicon.'"></a>';
			else
				$row[] = '(No photo)';

			$row[] = $ident->bahasa;

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_identitas('."'".$ident->id_identitas."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_identitas('."'".$ident->id_identitas."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Identitas_model->count_all(),
						"recordsFiltered" => $this->Identitas_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function save()
	{
		$this->_validate();

		$data = array(
            'nama_website'=>$this->input->post('nama_website'),
            'address'=>$this->input->post('address'),
            'email'=>$this->input->post('email'),
            'url'=>$this->input->post('url'),
            'facebook'=>$this->input->post('facebook'),
            'twitter'=>$this->input->post('twitter'),
            'youtube'=>$this->input->post('youtube'),
            'open'=>$this->input->post('open'),
            'no_telp'=>$this->input->post('no_telp'),
            'whatsapp'=>$this->input->post('whatsapp'),
            'meta_deskripsi'=>$this->input->post('meta_deskripsi'),
            'meta_keyword'=>$this->input->post('meta_keyword'),
            'maps'=>$this->input->post('maps'),
            'keterangan'=>$this->input->post('keterangan'),
            'moto'=>$this->input->post('moto'),
            'bahasa'=>$this->input->post('bahasa')
		);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			$data['favicon'] = $upload;
		}

		$insert = $this->Identitas_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	public function ajax_edit($id)
	{
		$data = $this->Identitas_model->get_by_id($id);
		echo json_encode($data);
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/images/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_website') == '')
		{
			$data['inputerror'][] = 'nama_website';
			$data['error_string'][] = 'Nama Website Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('address') == '')
		{
			$data['inputerror'][] = 'address';
			$data['error_string'][] = 'Address Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Email Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('url') == '')
		{
			$data['inputerror'][] = 'url';
			$data['error_string'][] = 'Url Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('open') == '')
		{
			$data['inputerror'][] = 'open';
			$data['error_string'][] = 'Opening Time Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('no_telp') == '')
		{
			$data['inputerror'][] = 'no_telp';
			$data['error_string'][] = 'No Telp Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('whatsapp') == '')
		{
			$data['inputerror'][] = 'whatsapp';
			$data['error_string'][] = 'No Hp Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('meta_deskripsi') == '')
		{
			$data['inputerror'][] = 'meta_deskripsi';
			$data['error_string'][] = 'Meta Deskripsi Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('meta_keyword') == '')
		{
			$data['inputerror'][] = 'meta_keyword';
			$data['error_string'][] = 'Meta Keyword Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('keterangan') == '')
		{
			$data['inputerror'][] = 'keterangan';
			$data['error_string'][] = 'Keterangan Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('moto') == '')
		{
			$data['inputerror'][] = 'moto';
			$data['error_string'][] = 'Moto Harus Diisi';
			$data['status'] = FALSE;
		}		

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function edit(){
        $this->_validate();

		$data = array(
			'nama_website'=>$this->input->post('nama_website'),
            'address'=>$this->input->post('address'),
            'email'=>$this->input->post('email'),
            'url'=>$this->input->post('url'),
            'facebook'=>$this->input->post('facebook'),
            'twitter'=>$this->input->post('twitter'),
            'youtube'=>$this->input->post('youtube'),
            'open'=>$this->input->post('open'),
            'no_telp'=>$this->input->post('no_telp'),
            'whatsapp'=>$this->input->post('whatsapp'),
            'meta_deskripsi'=>$this->input->post('meta_deskripsi'),
            'meta_keyword'=>$this->input->post('meta_keyword'),
            'maps'=>$this->input->post('maps'),
            'keterangan'=>$this->input->post('keterangan'),
            'moto'=>$this->input->post('moto'),
            'bahasa'=>$this->input->post('bahasa')
		);

		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('./asset/images/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('./asset/images/'.$this->input->post('remove_photo'));
			$data['favicon'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$ident = $this->Identitas_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/images/'.$ident->favicon) && $ident->favicon)
				unlink('./asset/images/'.$ident->favicon);

			$data['favicon'] = $upload;
		}


		$this->Identitas_model->update(array('id_identitas' => $this->input->post('id')), $data);
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function delete(){
		if($_POST['empid']) {
			$ident = $this->Identitas_model->get_by_id($_POST['empid']);
				unlink('./asset/images/'.$ident->favicon);
			$resultset = $this->Identitas_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Identitas */

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
