<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class IdentitasWebsite extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Funtion Action Identitas Website */
	public function index(){
		$this->data['title'] = 'Identitas Website';
		self::actionDashboard();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('identitasWebsite' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function save()
   	{
       	$this->form_validation->set_rules($this->MenuUtama_model->rules());
       	$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong');

        if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
        }else{
            $this->MenuUtama_model->do_upload();
            $response = ["status"=>'info','msg'=>"Data Berhasil Diperbarui<br>",'url'=>site_url('administrator/identitasWebsite')];
           	echo json_encode($response);
        }
    }
    /* End Funtion Action Identitas Website */

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
