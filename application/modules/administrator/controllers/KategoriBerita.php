<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class KategoriBerita extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Kategori Berita */
    public function index(){
		$this->data['title'] = 'Kategori Berita';
		self::actionDashboard();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('kategoriBerita' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_kategoriBerita(){
        if (isset($_GET['term'])) {
            $result = $this->KategoriBerita_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->nama_kategori;
                echo json_encode($arr_result);
            }
        }
    }

	public function kategoriBerita_ajax_list()
	{
		$list = $this->KategoriBerita_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $katBer) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $katBer->nama_kategori;
			$row[] = '<a href="'.base_url()."berita/kategori/".$katBer->kategori_seo.'" target="_blank">berita/kategori/'.$katBer->kategori_seo.'</a>';
			$row[] = $katBer->sidebar;
			if ($katBer->aktif == 'Y') {
				$status = "Aktif";
			}else{
				$status = "Tidak Aktif";
			}
			$row[] = $status;

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_kb('."'".$katBer->id_kategori."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_kb('."'".$katBer->id_kategori."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->KategoriBerita_model->count_all(),
						"recordsFiltered" => $this->KategoriBerita_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function export_pdfMW(){
		$data = array(
			'title' => 'Data Kategori Berita',
			//'list' => $this->KategoriBerita_model->getPerdana()
		);
	    $filename = "Bayu Setra Maulana";
	    $this->load->library('pdf');

	    $this->pdf->setPaper('A4', 'potrait');
	    $this->pdf->filename = $filename.".pdf";
		$this->pdf->load_view('menu_utama' . DIRECTORY_SEPARATOR . '_exportPdfMW', $data);

	}

	public function save()
	{
		$this->_validate();

		$data = array(
            'nama_kategori'=>$this->db->escape_str($this->input->post('nama_kategori')),
            'username'=>$this->session->username,
            'kategori_seo'=>seo_title($this->input->post('nama_kategori')),
            'aktif'=>$this->db->escape_str($this->input->post('status')),
            'sidebar'=>$this->db->escape_str($this->input->post('posisi'))
		);

		$insert = $this->KategoriBerita_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	public function ajax_edit($id)
	{
		$data = $this->KategoriBerita_model->get_by_id($id);
		echo json_encode($data);
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_kategori') == '')
		{
			$data['inputerror'][] = 'nama_kategori';
			$data['error_string'][] = 'Nama Kategori Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('posisi') == '')
		{
			$data['inputerror'][] = 'posisi';
			$data['error_string'][] = 'Posisi Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function edit(){
        $this->_validate();

		$data = array(
			'nama_kategori'=>$this->db->escape_str($this->input->post('nama_kategori')),
            'username'=>$this->session->username,
            'kategori_seo'=>seo_title($this->input->post('nama_kategori')),
            'aktif'=>$this->db->escape_str($this->input->post('status')),
            'sidebar'=>$this->db->escape_str($this->input->post('posisi'))
		);

		$this->KategoriBerita_model->update(array('id_kategori' => $this->input->post('id')), $data);
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function delete(){
		if($_POST['empid']) {
			$resultset = $this->KategoriBerita_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Kategori Berita */

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
