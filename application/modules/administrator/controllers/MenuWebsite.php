<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class MenuWebsite extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Menu Website */
    public function index(){
		$this->data['title'] = 'Menu Website';
		self::actionDashboard();
		$this->data['menu_utama'] = $this->MenuWebsite_model->menu_utama();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('menuWebsite' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_menuWebsite(){
        if (isset($_GET['term'])) {
            $result = $this->MenuWebsite_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->nama_menu;
                echo json_encode($arr_result);
            }
        }
    }

	public function menuWebsite_ajax_list()
	{
		$list = $this->MenuWebsite_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $menuwebsite) {
			$cmenu = $this->MenuWebsite_model->menu_cek($menuwebsite->id_parent)->row_array();
			if ($cmenu['id_parent']=='') {$menu = 'Menu Utama';}else{$menu = $cmenu['nama_menu'];}
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $menuwebsite->nama_menu;
			$row[] = $menu;
			$row[] = '<a href="'.base_url().$menuwebsite->link.'" target="_blank">'.$menuwebsite->link.'</a>';

			if ($menuwebsite->aktif == 'Ya') {
				$status = "Aktif";
			}else{
				$status = "Tidak Aktif";
			}
			$row[] = $status;
			$row[] = $menuwebsite->position;
			$row[] = $menuwebsite->urutan;
			$row[] = $menuwebsite->bahasa;

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_mw('."'".$menuwebsite->id_menu."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_mw('."'".$menuwebsite->id_menu."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->MenuWebsite_model->count_all(),
						"recordsFiltered" => $this->MenuWebsite_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function export_pdfMW(){
		$data = array(
			'title' => 'Data Menu Website',
			//'list' => $this->MenuWebsite_model->getPerdana()
		);
	    $filename = "Bayu Setra Maulana";
	    $this->load->library('pdf');

	    $this->pdf->setPaper('A4', 'potrait');
	    $this->pdf->filename = $filename.".pdf";
		$this->pdf->load_view('menu_utama' . DIRECTORY_SEPARATOR . '_exportPdfMW', $data);

	}

	public function saveMenuWebsite()
	{
		$this->_validate();

		$data = array(
			'link' => $this->db->escape_str($this->input->post('link_menu')),
			'id_parent' => $this->db->escape_str($this->input->post('level_menu')),
            'nama_menu' => $this->db->escape_str($this->input->post('nama_menu')),
            'aktif' => $this->db->escape_str($this->input->post('status')),
            'position' => $this->db->escape_str($this->input->post('posisi')),
            'urutan' => $this->db->escape_str($this->input->post('urutan')),
            'bahasa' => $this->db->escape_str($this->input->post('bahasa'))
		);

		$insert = $this->MenuWebsite_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	public function ajax_edit($id)
	{
		$data = $this->MenuWebsite_model->get_by_id($id);
		echo json_encode($data);
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('link_menu') == '')
		{
			$data['inputerror'][] = 'link_menu';
			$data['error_string'][] = 'Link Menu Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('level_menu') == '')
		{
			$data['inputerror'][] = 'level_menu';
			$data['error_string'][] = 'Level Menu Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('nama_menu') == '')
		{
			$data['inputerror'][] = 'nama_menu';
			$data['error_string'][] = 'Nama Menu Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('posisi') == '')
		{
			$data['inputerror'][] = 'posisi';
			$data['error_string'][] = 'Posisi Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('status') == '')
		{
			$data['inputerror'][] = 'status';
			$data['error_string'][] = 'Status Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('urutan') == '')
		{
			$data['inputerror'][] = 'urutan';
			$data['error_string'][] = 'urutan Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function editMenuWebsite(){
        $this->_validate();

		$data = array(
			'link' => $this->db->escape_str($this->input->post('link_menu')),
			'id_parent' => $this->db->escape_str($this->input->post('level_menu')),
            'nama_menu' => $this->db->escape_str($this->input->post('nama_menu')),
            'aktif' => $this->db->escape_str($this->input->post('status')),
            'position' => $this->db->escape_str($this->input->post('posisi')),
            'urutan' => $this->db->escape_str($this->input->post('urutan')),
            'bahasa' => $this->db->escape_str($this->input->post('bahasa'))
		);

		$this->MenuWebsite_model->update(array('id_menu' => $this->input->post('id')), $data);
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function deleteMenu(){
		if($_POST['empid']) {
			$resultset = $this->MenuWebsite_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Menu Website */

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
