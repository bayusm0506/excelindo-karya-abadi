<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Modul_berita extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	//Rules List Berita
	private function rules(){
		return $this->ListBerita_model->rules();
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

	/* Funtion Action Modul Berita -> List Berita */
	public function index(){
		$this->data['title'] = 'List Berita';
		self::actionDashboard();
		$this->data['record'] = $this->ListBerita_model->listBerita(0)->result_array();
		$this->data['kategori_berita'] = $this->KategoriBerita_model->kategoriBerita()->result_array();
		$this->data['tag'] = $this->TagBerita_model->tagBerita()->result_array();
		$this->data['tag_edit'] = $this->TagBerita_model->tagBerita()->result_array();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('modul_berita' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_listBerita(){
        if (isset($_GET['term'])) {
            $result = $this->ListBerita_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->judul;
                echo json_encode($arr_result);
            }
        }
    }

	public function listBerita_ajax_list()
	{
		$list = $this->ListBerita_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $listberita) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $listberita->judul;
			$row[] = tgl_indo($listberita->tanggal);
			if ($listberita->status == 'Y') {
				$status = "<span style='color:green'>Published</span>";
			}else{
				$status = "<span style='color:red'>Unpublished</span>";
			}
			$row[] = $status;
			$row[] = '
				<a class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_edit'.$listberita->id_berita.'" data-toggle="tooltip" data-placement="bottom" title="Perbarui Data"><span class="glyphicon glyphicon-edit"></span>Update</a>
            <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_menugroup('."'".$listberita->id_berita."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>
			';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->ListBerita_model->count_all(),
						"recordsFiltered" => $this->ListBerita_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function saveListBerita(){
       	$this->form_validation->set_rules($this->ListBerita_model->rules());
       	$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong');

        if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
           	//echo json_encode(['error'=>$errors]);
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
        }else{
        	$this->ListBerita_model->save();
            $response = ["status"=>'ok','msg'=>'Data Berhasil di Simpan.'];
            echo json_encode($response);
           	//echo json_encode(['success'=>'Form submitted successfully.']);
        }
	}

	public function editListBerita(){
       	$this->form_validation->set_rules($this->ListBerita_model->rules());
       	$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong');

        if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
           	//echo json_encode(['error'=>$errors]);
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
        }else{
        	$this->ListBerita_model->update();
            $response = ["status"=>'info','msg'=>'Data Berhasil di Perbarui.'];
            echo json_encode($response);
           	//echo json_encode(['success'=>'Form submitted successfully.']);
        }
	}

	public function deleteListBerita(){
		if($_POST['empid']) {
			$berita = $this->ListBerita_model->get_by_id($_POST['empid']);
			if(file_exists('./asset/foto_berita/'.$berita->gambar) && $berita->gambar)
				unlink('./asset/foto_berita/'.$berita->gambar);
			$resultset = $this->ListBerita_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Funtion Action Modul Berita -> List Berita */

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}