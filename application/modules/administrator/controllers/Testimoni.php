<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Testimoni extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Testimoni */
    public function index(){
		$this->data['title'] = 'Testimoni';
		self::actionDashboard();
		
		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('testimoni' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->Testimoni_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->nama;
                echo json_encode($arr_result);
            }
        }
    }

	public function ajax_list()
	{
		$list = $this->Testimoni_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $testi) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $testi->nama;
			$row[] = $testi->website;
			$row[] = tgl_indo($testi->tgl_posting);
			if ($testi->status == 'Y') {
				$status = "Aktif";
			}else{
				$status = "Tidak Aktif";
			}

			$row[] = $status;
			
			if($testi->gambar)
				$row[] = '<a href="'.base_url('./asset/img_testi/').$testi->gambar.'" target="_blank"><img width="40" height="40" class="img-circle" src="'.base_url('./asset/img_testi/').$testi->gambar.'"></a>';
			else
				$row[] = '(No photo)';

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_testi('."'".$testi->id_testi."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_testi('."'".$testi->id_testi."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Testimoni_model->count_all(),
						"recordsFiltered" => $this->Testimoni_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function save(){

        $this->_validate();

		$data = array(
            'nama'=>$this->db->escape_str($this->input->post('nama')),
            'website'=>$this->db->escape_str($this->input->post('website')),
            'status'=>$this->db->escape_str($this->input->post('status')),
          	'keterangan'=>$this->input->post('keterangan'),
          	'tgl_posting'=>date('Y-m-d'),
          	'username'=>$this->session->username,
          	'jam'=>date('H:i:s'),
          	'hari'=>hari_ini(date('w'))
		);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			$data['gambar'] = $upload;
		}

		$insert = $this->Testimoni_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/img_testi/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama') == '')
		{
			$data['inputerror'][] = 'nama';
			$data['error_string'][] = 'Nama Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('status') == '')
		{
			$data['inputerror'][] = 'status';
			$data['error_string'][] = 'Status Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_edit($id)
	{
		$data = $this->Testimoni_model->get_by_id($id);
		echo json_encode($data);
	}

	public function edit(){
        $this->_validate();
		$data = array(
			'nama'=>$this->db->escape_str($this->input->post('nama')),
			'website'=>$this->db->escape_str($this->input->post('website')),
			'status'=>$this->db->escape_str($this->input->post('status')),
          	'keterangan'=>$this->input->post('keterangan'),
          	'tgl_posting'=>date('Y-m-d'),
          	'username'=>$this->session->username,
          	'jam'=>date('H:i:s'),
          	'hari'=>hari_ini(date('w'))
		);

		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('./asset/img_testi/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('./asset/img_testi/'.$this->input->post('remove_photo'));
			$data['gambar'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();

			//delete file
			$testi = $this->Testimoni_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/img_testi/'.$testi->gambar) && $testi->gambar)
				unlink('./asset/img_testi/'.$testi->gambar);

			$data['gambar'] = $upload;
		}

		$this->Testimoni_model->update(array('id_testi' => $this->input->post('id')), $data);
		//echo json_encode(array("status" => TRUE));
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	public function delete(){
		if($_POST['empid']) {
			$testi = $this->Testimoni_model->get_by_id($_POST['empid']);
				unlink('./asset/img_testi/'.$testi->gambar);
			$resultset = $this->Testimoni_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Testimoni*/

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
