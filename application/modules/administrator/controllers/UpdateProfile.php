<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class UpdateProfile extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Funtion Action User */
	public function index(){
		$this->data['title'] = 'Update Profile';
		self::actionDashboard();

		$this->data['profile'] = $this->User_model->profile($this->session->user_id)->row_array();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('user' . DIRECTORY_SEPARATOR . '_update_profile', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function save()
   	{
       	//$this->form_validation->set_rules($this->User_model->rules());
       	//$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong');
   		// $cek_email = $this->User_model->profile($this->input->post('email'))->num_rows();
     //   	if ($cek_email > 0) {
     //   		$response = ["status"=>'error','msg'=>"Email Sudah Ada yang memakai"];
     //       	echo json_encode($response);
     //   	}

   		if ($this->input->post('password') != '') {
   			if ($this->input->post('password') != $this->input->post('confirm_password')) {
	   			$response = ["status"=>'error','msg'=>"Password Tidak Sama"];
	           	echo json_encode($response);
	   		}else{
	   			$hashed_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
				//var_dump($hashed_password);
	   			$data = array(
	   				'first_name' => $this->input->post('first_name'),
	   				'last_name' => $this->input->post('last_name'),
	   				'company' => $this->input->post('company'),
	   				'phone' => $this->input->post('phone'),
	   				'password' => $hashed_password
	   			);
	   			$this->User_model->update(array('id' => $this->session->user_id), $data);
	            $response = ["status"=>'info','msg'=>"Data Berhasil Diperbarui<br>",'url'=>site_url('administrator/updateProfile')];
	           	echo json_encode($response);
	   		}
   		}else{
   			$data = array(
   				'first_name' => $this->input->post('first_name'),
   				'last_name' => $this->input->post('last_name'),
   				'company' => $this->input->post('company'),
   				'phone' => $this->input->post('phone')
   			);

   			$this->User_model->update(array('id' => $this->session->user_id), $data);
            $response = ["status"=>'info','msg'=>"Data Berhasil Diperbarui<br>",'url'=>site_url('administrator/updateProfile')];
           	echo json_encode($response);
   		}

   		

        // if ($this->form_validation->run() == FALSE){
        //     $errors = validation_errors();
        //     $response = ["status"=>'error','msg'=>$errors];
        //     echo json_encode($response);
        // }else{
        //     $this->User_model->do_upload();
        //     $response = ["status"=>'info','msg'=>"Data Berhasil Diperbarui<br>",'url'=>site_url('administrator/identitasWebsite')];
        //    	echo json_encode($response);
        // }
    }
    /* End Funtion Action User */

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
