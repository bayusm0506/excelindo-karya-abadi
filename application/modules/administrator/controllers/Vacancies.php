<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Vacancies extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Vacancies */
    public function index(){
		$this->data['title'] = 'Vacancies';
		self::actionDashboard();
		$this->data['position'] = $this->Position_model->getCountPosition()->result_array();
		
		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('vacancies' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

    public function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->Vacancies_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->name;
                echo json_encode($arr_result);
            }
        }
    }

	public function ajax_list()
	{
		$list = $this->Vacancies_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $vac) {
			$cmenu = $this->Position_model->menu_cek($vac->position)->row_array();
			if ($cmenu['id_position']=='') {$menu = '#';}else{$menu = $cmenu['position'];}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $vac->name;
			$row[] = $vac->phone;
			$row[] = $vac->email;
			$row[] = $menu;
			$row[] = tgl_indo($vac->tgl_posting);
			if ($vac->verified == 'Y') {
				$row[] = "<button class='btn btn-info'>Sudah Diverifikasi</button>";
			}else{
				$row[] = "<button class='btn btn-danger'>Belum Diverifikasi</button>";
			}

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_vac('."'".$vac->id_vacancies."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_vac('."'".$vac->id_vacancies."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

			 //  $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="view_vac('."'".$vac->id_vacancies."'".')"><i class="glyphicon glyphicon-user"></i> View</a>
				// <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_vac('."'".$vac->id_vacancies."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
			 //  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_vac('."'".$vac->id_vacancies."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Vacancies_model->count_all(),
						"recordsFiltered" => $this->Vacancies_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function save(){

        $this->_validate();

		$data = array(
          	'name'=>$this->input->post('name'),
          	'phone'=>$this->input->post('phone'),
          	'email'=>$this->input->post('email'),
          	'position'=>$this->input->post('position'),
          	'verified'=>$this->input->post('verified'),
          	'tgl_posting'=>date('Y-m-d'),
          	'jam'=>date('H:i:s'),
          	'hari'=>hari_ini(date('w'))
		);

		if(!empty($_FILES['attach1']['name']))
		{
			$upload = $this->_do_upload_attach1();
			$data['attachment1'] = $upload;
		}

		if(!empty($_FILES['attach2']['name']))
		{
			$upload = $this->_do_upload_attach2();
			$data['attachment2'] = $upload;
		}

		if(!empty($_FILES['attach3']['name']))
		{
			$upload = $this->_do_upload_attach3();
			$data['attachment3'] = $upload;
		}

		if(!empty($_FILES['attach4']['name']))
		{
			$upload = $this->_do_upload_attach4();
			$data['attachment4'] = $upload;
		}

		$insert = $this->Vacancies_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	private function _do_upload_attach1()
	{
		$config['upload_path']          = './asset/file_vacancies/';
        $config['allowed_types']        = 'gif|jpg|png|zip|rar';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('attach1')) //upload and validate
        {
            $data['inputerror'][] = 'attach1';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _do_upload_attach2()
	{
		$config['upload_path']          = './asset/file_vacancies/';
        $config['allowed_types']        = 'gif|jpg|png|zip|rar';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('attach2')) //upload and validate
        {
            $data['inputerror'][] = 'attach2';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _do_upload_attach3()
	{
		$config['upload_path']          = './asset/file_vacancies/';
        $config['allowed_types']        = 'gif|jpg|png|zip|rar';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('attach3')) //upload and validate
        {
            $data['inputerror'][] = 'attach3';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _do_upload_attach4()
	{
		$config['upload_path']          = './asset/file_vacancies/';
        $config['allowed_types']        = 'gif|jpg|png|zip|rar';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('attach4')) //upload and validate
        {
            $data['inputerror'][] = 'attach4';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name') == '')
		{
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Nama Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		if($this->input->post('phone') == '')
		{
			$data['inputerror'][] = 'phone';
			$data['error_string'][] = 'No HP Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Email Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		if($this->input->post('position') == '')
		{
			$data['inputerror'][] = 'position';
			$data['error_string'][] = 'Position Tidak Boleh Kosong';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_edit($id)
	{
		$data = $this->Vacancies_model->get_by_id($id);
		echo json_encode($data);
	}

	public function edit(){
        $this->_validate();
		$data = array(
          	'name'=>$this->input->post('name'),
          	'phone'=>$this->input->post('phone'),
          	'email'=>$this->input->post('email'),
          	'position'=>$this->input->post('position'),
          	'jam'=>date('H:i:s'),
          	'hari'=>hari_ini(date('w')),
          	'verified'=>$this->input->post('verified')
		);

		if($this->input->post('remove_attach1')) // if remove photo checked
		{
			if(file_exists('./asset/file_vacancies/'.$this->input->post('remove_attach1')) && $this->input->post('remove_attach1'))
				unlink('./asset/file_vacancies/'.$this->input->post('remove_attach1'));
			$data['attachment1'] = '';
		}

		if($this->input->post('remove_attach2')) // if remove photo checked
		{
			if(file_exists('./asset/file_vacancies/'.$this->input->post('remove_attach2')) && $this->input->post('remove_attach2'))
				unlink('./asset/file_vacancies/'.$this->input->post('remove_attach2'));
			$data['attachment2'] = '';
		}

		if($this->input->post('remove_attach3')) // if remove photo checked
		{
			if(file_exists('./asset/file_vacancies/'.$this->input->post('remove_attach3')) && $this->input->post('remove_attach3'))
				unlink('./asset/file_vacancies/'.$this->input->post('remove_attach3'));
			$data['attachment3'] = '';
		}

		if($this->input->post('remove_attach4')) // if remove photo checked
		{
			if(file_exists('./asset/file_vacancies/'.$this->input->post('remove_attach4')) && $this->input->post('remove_attach4'))
				unlink('./asset/file_vacancies/'.$this->input->post('remove_attach4'));
			$data['attachment4'] = '';
		}

		if(!empty($_FILES['attach1']['name']))
		{
			$upload = $this->_do_upload_attach1();

			//delete file
			$vac = $this->Vacancies_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/file_vacancies/'.$vac->attachment1) && $vac->attachment1)
				unlink('./asset/file_vacancies/'.$vac->attachment1);

			$data['attachment1'] = $upload;
		}

		if(!empty($_FILES['attach2']['name']))
		{
			$upload = $this->_do_upload_attach2();

			//delete file
			$vac = $this->Vacancies_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/file_vacancies/'.$vac->attachment2) && $vac->attachment2)
				unlink('./asset/file_vacancies/'.$vac->attachment2);

			$data['attachment2'] = $upload;
		}

		if(!empty($_FILES['attach3']['name']))
		{
			$upload = $this->_do_upload_attach3();

			//delete file
			$vac = $this->Vacancies_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/file_vacancies/'.$vac->attachment3) && $vac->attachment3)
				unlink('./asset/file_vacancies/'.$vac->attachment3);

			$data['attachment3'] = $upload;
		}

		if(!empty($_FILES['attach4']['name']))
		{
			$upload = $this->_do_upload_attach4();

			//delete file
			$vac = $this->Vacancies_model->get_by_id($this->input->post('id'));
			
			if(file_exists('./asset/file_vacancies/'.$vac->attachment4) && $vac->attachment4)
				unlink('./asset/file_vacancies/'.$vac->attachment4);

			$data['attachment4'] = $upload;
		}

		$this->Vacancies_model->update(array('id_vacancies' => $this->input->post('id')), $data);
		//echo json_encode(array("status" => TRUE));
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	// public function verified(){
	// 	$data = array(
 //          	'verified'=>$this->input->post('verified'),
 //          	'position'=>$this->input->post('position'),
	// 	);

	// 	$this->Vacancies_model->update(array('id_vacancies' => $this->input->post('id')), $data);
	// 	//echo json_encode(array("status" => TRUE));
	// 	echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diverifikasi"));
	// }

	public function delete(){
		if($_POST['empid']) {
			$vac = $this->Vacancies_model->get_by_id($_POST['empid']);
				unlink('./asset/file_vacancies/'.$vac->attachment1);
				unlink('./asset/file_vacancies/'.$vac->attachment2);
				unlink('./asset/file_vacancies/'.$vac->attachment3);
				unlink('./asset/file_vacancies/'.$vac->attachment4);
			$resultset = $this->Vacancies_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Function Action Vacancies*/

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
