<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Video extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

	/* Funtion Action Video */
	public function index(){
		$this->data['title'] = 'Video';
		self::actionDashboard();
		$this->data['playlist'] = $this->Playlist_model->getPlaylist()->result_array();
		$this->data['tagvid'] = $this->TagVideo_model->getTagVideo()->result_array();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('video' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function update(){
		$this->data['title'] = 'Video';
		self::actionDashboard();
		$id = $this->uri->segment(4);
		$this->data['playlist'] = $this->Playlist_model->getPlaylist()->result_array();
		$this->data['tagvid'] = $this->TagVideo_model->getTagVideo()->result_array();
		$this->data['rows'] = $this->Video_model->get_by_id_edit($id)->row_array();

		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('video' . DIRECTORY_SEPARATOR . '_form_update', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function get_autocomplete_judul(){
        if (isset($_GET['term'])) {
            $result = $this->Video_model->autocomplete($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->jdl_video;
                echo json_encode($arr_result);
            }
        }
    }

	public function ajax_list()
	{
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$list = $this->Video_model->get_datatables($row['name']);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $vid) {
			$cmenu = $this->Playlist_model->menu_cek($vid->id_playlist)->row_array();
			if ($cmenu['id_playlist']=='') {$menu = 'No Playlist';}else{$menu = $cmenu['jdl_playlist'];}
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $vid->jdl_video;
			$row[] = tgl_indo($vid->tanggal);
			$row[] = $menu;

			$row[] = '<a class="btn btn-sm btn-primary" href="'.base_url().'administrator/video/update/'.$vid->id_video.'" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_vid('."'".$vid->id_video."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Video_model->count_all(),
						"recordsFiltered" => $this->Video_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function save(){
        $this->_validate();

	    if ($this->input->post('tag') != ''){
	        $tag_seo = $this->input->post('tag');
	        $tag=implode(',',$tag_seo);
	    }else{
	        $tag = '';
	    }

		$data = array(
          	'id_playlist'=>$this->input->post('id_playlist'),
            'username'=>$this->session->username,
            'jdl_video'=>$this->input->post('jdl_video'),
            'video_seo'=>seo_title($this->input->post('jdl_video')),
            'keterangan'=>$this->input->post('keterangan'),
            'video'=>'',
            'youtube'=>$this->input->post('youtube'),
            'dilihat'=>'0',
            'hari'=>hari_ini(date('w')),
            'tanggal'=>date('Y-m-d'),
            'jam'=>date('H:i:s'),
            'tagvid'=>$tag
		);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			$data['gbr_video'] = $upload;
		}

		$insert = $this->Video_model->save($data);

		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Disimpan"));
	}

	private function _do_upload()
	{
		$config['upload_path']          = './asset/img_video/';
        $config['allowed_types']        = 'gif|jpg|png|JPG|JPEG';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _do_upload_update()
	{
		$config['upload_path']          = './asset/img_video/';
        $config['allowed_types']        = 'gif|jpg|png|JPG|JPEG';
        $config['max_size']             = 3000; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
			$errors = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			// $response = ["status"=>'info','msg'=>$errors];
	  //       echo json_encode($response);

	        echo json_encode(array("status" => 'info', 'msg'=>$errors));
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('jdl_video') == '')
		{
			$data['inputerror'][] = 'jdl_video';
			$data['error_string'][] = 'Judul Video Harus Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('id_playlist') == '')
		{
			$data['inputerror'][] = 'id_playlist';
			$data['error_string'][] = 'Playlist Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function edit(){
		$this->_do_upload_update();
		
       	$this->form_validation->set_rules($this->Video_model->rules());
       	$this->form_validation->set_message('required', '{field} Tidak Boleh Kosong');

        if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
            $response = ["status"=>'error','msg'=>$errors];
            echo json_encode($response);
        }else{
		    if ($this->input->post('tag') != ''){
		        $tag_seo = $this->input->post('tag');
		        $tag=implode(',',$tag_seo);
		    }else{
		        $tag = '';
		    }

			$data = array(
	          	'id_playlist'=>$this->input->post('id_playlist'),
	            'username'=>$this->session->username,
	            'jdl_video'=>$this->input->post('jdl_video'),
	            'video_seo'=>seo_title($this->input->post('jdl_video')),
	            'keterangan'=>$this->input->post('keterangan'),
	            'video'=>'',
	            'youtube'=>$this->input->post('youtube'),
	            'dilihat'=>'0',
	            'hari'=>hari_ini(date('w')),
	            'tanggal'=>date('Y-m-d'),
	            'jam'=>date('H:i:s'),
	            'tagvid'=>$tag
			);

			if($this->input->post('remove_photo')) // if remove photo checked
			{
				if(file_exists('./asset/img_video/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
					unlink('./asset/img_video/'.$this->input->post('remove_photo'));
				$data['gbr_video'] = '';
			}

			if(!empty($_FILES['photo']['name']))
			{
				$upload = $this->_do_upload_update();

				//delete file
				$vid = $this->Video_model->get_by_id($this->input->post('id'));
				
				if(file_exists('./asset/img_video/'.$vid->gbr_video) && $vid->gbr_video)
					unlink('./asset/img_video/'.$vid->gbr_video);

				$data['gbr_video'] = $upload;
			}


			$this->Video_model->update(array('id_video' => $this->input->post('id')), $data);

			$response = ["status"=>'info','msg'=>'Data Berhasil di Perbarui.','url'=>site_url('administrator/video/update/'.$this->input->post('id'))];
	        echo json_encode($response);
        }
	}

	public function delete(){
		if($_POST['empid']) {
			$vid = $this->Video_model->get_by_id($_POST['empid']);
			if(file_exists('./asset/img_video/'.$vid->gbr_video) && $vid->gbr_video)
				unlink('./asset/img_video/'.$vid->gbr_video);
			$resultset = $this->Video_model->delete($_POST['empid']);
			if($resultset) {
				echo "Record Deleted";
			}
		}
	}
	/* End Funtion Action Video */

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}