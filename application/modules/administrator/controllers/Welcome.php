<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Welcome extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->Ion_auth_model->Authenticate();
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('administrator/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//Success
		}
	}

	private function actionDashboard(){
		$cek = $this->Dashboard_model->getUserDescription($this->session->id_groups);
		$row = $cek->row_array();
		$this->data['first_name'] = $this->session->first_name;
		$this->data['description'] = $row['description'];
		$this->data['info_messages'] = $this->Dashboard_model->info_getMessages();
		$this->data['messages'] = $this->Dashboard_model->new_message(10);
		$this->data['record'] = $this->MenuUtama_model->identitas()->row_array();
		$this->data['fav'] = $this->MenuUtama_model->favicon()->row_array();
	}

    /* Function Action Welcome and About */
    public function index(){
		$this->data['title'] = 'Welcome and About';
		self::actionDashboard();		
		$this->_render_page('layouts/main_header', $this->data);
		$this->_render_page('welcome' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->_render_page('layouts/main_footer', $this->data);
	}

	public function ajax_list()
	{
		$list = $this->Welcome_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $welcome) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $welcome->welcome;
			$row[] = $welcome->about;

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_wel('."'".$welcome->id."'".')"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Welcome_model->count_all(),
						"recordsFiltered" => $this->Welcome_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('welcome') == '')
		{
			$data['inputerror'][] = 'welcome';
			$data['error_string'][] = 'Welcome Harus Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_edit($id)
	{
		$data = $this->Welcome_model->get_by_id($id);
		$data->welcome = ($data->welcome == '...') ? '' : $data->welcome;
		echo json_encode($data);
	}

	public function edit(){
        $this->_validate();
		$data = array(
			'welcome'=>$this->input->post('welcome'),
			'about'=>$this->input->post('about')
		);

		$this->Welcome_model->update(array('id' => $this->input->post('id')), $data);
		//echo json_encode(array("status" => TRUE));
		echo json_encode(array("status" => 'info', 'msg'=>"Data Berhasil Diperbarui"));
	}

	/* End Function Action Welcome and About*/

    /**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		//$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);
		//$view_html = $this->template->load('template', $view, $this->viewdata, $returnhtml);
		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}
