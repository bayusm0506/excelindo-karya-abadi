<div class="box-body">
  <div class="row">
    <div class="col-md-12">
    	<div class="alert alert-danger" style="display:none"></div>
    	<div class="alert alert-info" style="display:none"></div>
    	<?php echo form_open('administrator/dashboard/validateForm', array('id'=>'table-news', 'role'=>'form'));?>
			<div class="form-group">
				<select class="select2 form-control" name="kategori">
		            <option value="">Pilih Kategori</option>
		            <?php
			            if ($kategori->num_rows() > 0)
						{
						   foreach ($kategori->result() as $row)
						   {
					?>
						<option value="<?=$row->id_kategori?>"><?=$row->nama_kategori?></option>		
					<?php
						   }
						}
					?>
		         </select>
			</div>
			<div class="form-group">
				<?php echo form_input('judul', '', array('class' => 'form-control', 'placeholder' => 'Judul Berita', 'id' => 'input-judul'));?>	
			</div>
			<div class="form-group">
			  <textarea name='isi' placeholder="Isi Berita..." id="isi" style="width: 100%; height: 169px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
			</div>
			<div class="form-group">
				<button type='submit' class="pull-right btn btn-info btn-submit">Submit <i class="fa fa-arrow-circle-right"></i></button>
			</div>
		<?php echo form_close();?>
      <!-- /.form-group -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
<!-- /.box-body -->
<script>
	CKEDITOR.replace('isi' ,{
	   	filebrowserImageBrowseUrl : '<?php echo base_url(); ?>asset/kcfinder'
	});
    $("form#table-news").submit(function(){  
	  CKEDITOR.instances.isi.updateElement();
      var _form = $(this);
      //_form += $(this).val(CKEDITOR.instances[$(this).attr('isi')].getData());
      Component.run(_form.attr('action'),'POST',_form.serialize(),function(response){
        // Pesan
        if(response.status == 'error'){
          //Message
        }else if(response.status == 'info'){
        	$('form#table-news')[0].reset();
          //window.setTimeout(window.location.href = response.redirect,50000);
        }
      });
      return false;
    });
</script>