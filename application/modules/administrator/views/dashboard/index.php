<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	  <div class="row">
	    <div class="col-md-3 col-sm-6 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-aqua"><i class="fa fa-newspaper-o"></i></span>

	        <div class="info-box-content">
	          <span class="info-box-text">BERITA</span>
	          <span class="info-box-number" style="font-size: 28px;" id="news"><small></small></span>
	          <span class="info-box-text"><a href="<?php echo base_url(); ?>administrator/listberita" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a></span>
	        </div>

	        
	        <!-- /.info-box-content -->
	      </div>
	      <!-- /.info-box -->
	    </div>
	    <!-- /.col -->
	    <div class="col-md-3 col-sm-6 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-red"><i class="fa fa-folder-open-o"></i></span>

	        <div class="info-box-content">
	          <span class="info-box-text">HALAMAN</span>
	          <span class="info-box-number" style="font-size: 28px;" id="pages"></span>
	          <span class="info-box-text"><a href="<?php echo base_url(); ?>administrator/halamanbaru" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a></span>
	        </div>
	        <!-- /.info-box-content -->
	      </div>
	      <!-- /.info-box -->
	    </div>
	    <!-- /.col -->

	    <!-- fix for small devices only -->
	    <div class="clearfix visible-sm-block"></div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-green"><i class="fa fa-calendar"></i></span>

	        <div class="info-box-content">
	          <span class="info-box-text">AGENDA</span>
	          <span class="info-box-number" style="font-size: 28px;" id="agenda"></span>
	          <span class="info-box-text"><a href="<?php echo base_url(); ?>administrator/agenda" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a></span>
	        </div>
	        <!-- /.info-box-content -->
	      </div>
	      <!-- /.info-box -->
	    </div>
	    <!-- /.col -->
	    <div class="col-md-3 col-sm-6 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>

	        <div class="info-box-content">
	          <span class="info-box-text">USERS</span>
	          <span class="info-box-number" style="font-size: 28px;" id="users_info"></span>
	          <span class="info-box-text"><a href="<?php echo base_url(); ?>administrator/manajemenuser" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a></span>
	        </div>
	        <!-- /.info-box-content -->
	      </div>
	      <!-- /.info-box -->
	    </div>
	    <!-- /.col -->
	  </div>
	  <!-- /.row -->
	<div class="row">
	    <div class="col-md-6">
			<!-- general form elements -->
	          <!-- SELECT2 EXAMPLE -->
	      <div class="box box-default">
	        <div class="box-header with-border">
			  <i class="fa fa-area-chart"></i>
	          <h3 class="box-title">Grafik Kunjungan</h3>

	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
	          </div>
	        </div>
	        <!-- /.box-header -->
	        <div class="box-body">
	          <div class="row">
	            <div class="col-md-12">
				  <?php $this->load->view('dashboard/_grafik',array('grafik'=>$grafik)); ?>
	              <!-- /.form-group -->
	            </div>
	            <!-- /.col -->
	          </div>
	          <!-- /.row -->
	        </div>
	        <!-- /.box-body -->
	      </div>
	      <!-- /.box -->
	          <!-- /.box -->
		</div>
		<div class="col-md-6">
			<!-- general form elements -->
	      <div class="box box-default">
	        <div class="box-header with-border">
			  <i class="fa fa-pencil"></i>
	          <h3 class="box-title">Tulis Berita Secara Cepat</h3>

	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
	          </div>
	        </div>
	        <!-- /.box-header -->
	        <?php $this->load->view('dashboard/_news'); ?>
	      </div>
	      <!-- /.box -->
	          <!-- /.box -->
		</div>
	</div>
  <!-- /.row -->
</section>
<!-- /.content -->

<script>
	var auto_get_info = function(){
		Ajax.run("<?=site_url('administrator/dashboard/RealData')?>", 'GET', {},function(response){
			$("#news").text(response.news);
			$("#pages").text(response.pages);
			$("#agenda").text(response.agenda);
			$("#users_info").text(response.users_info);

			$("#element_chart_data").html("");
			
			var yAxRealisasi = [
				{ // Primary yAxis
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    title: {
                        text: 'Jumlah Kunjungan',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }
                }
                , 
                { // Secondary yAxis
                    title: {
                        text: 'Jumlah Hits',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    labels: {
                        //format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    
                    opposite: true
                }
                ];
			var _response1 = response.realChart;
          //,$category,$yAxis,$series,$title,$subtitle,$text
          Maps.multyAxis("element_chart_data",_response1.categories,yAxRealisasi,_response1.series,_response1.title,_response1.subtitle,_response1.text);
        });
	}
	var runInt = setInterval(function(){ auto_get_info(); }, 7000);

	$(document).ready(function(){
		auto_get_info();
	})
</script>