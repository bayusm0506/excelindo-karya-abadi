<div class="box-body">
  <table id="table" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>No</th>
      <th>Judul Event</th>
      <th>Tanggal Event</th>
      <th>Waktu Mulai - Selesai</th>
      <th>Status</th>
      <th>Photo</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      
    </tbody>
    <tfoot>
    <tr>
      <th>No</th>
      <th>Judul Event</th>
      <th>Tanggal Event</th>
      <th>Waktu Mulai - Selesai</th>
      <th>Status</th>
      <th>Photo</th>
      <th>Action</th>
    </tr>
    </tfoot>
  </table>
</div>
<!-- /.box-body -->