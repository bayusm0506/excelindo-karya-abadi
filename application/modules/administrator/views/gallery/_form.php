<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" style="width:950px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Gallery Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <div class="callout callout-info">
                    <h4><i class="fa fa-info"></i> Note:</h4>
                        1. Max File Upload : 3 Mb <br>
                        2. Format Extensi File : gif,jpg,png <br>
                        3. File Upload = Width : 800px, Height : 625px <br>
                    </div>
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Judul Foto</label>
                            <div class="col-md-9">
                                <input name="jdl_gallery" placeholder="Judul Album" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Album</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_album" style="width:100%;" id="id_album_f">
                                  <option value="">Pilih Album</option>
                                  <?php
                                    if ($album->num_rows() > 0)
                                    {
                                       foreach ($album->result() as $row)
                                       {
                                  ?>
                                    <option value="<?=$row->id_album?>"><?=$row->jdl_album?></option>    
                                  <?php
                                       }
                                    }
                                  ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="keterangan-preview">
                            <label class="control-label col-md-3">Keterangan</label>
                            <div class="col-md-9">
                                
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="photo-preview">
                            <label class="control-label col-md-3">Photo 1</label>
                            <div class="col-md-9">
                                (No photo)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" id="label-photo">Photo 1</label>
                            <div class="col-md-9">
                                <input type="file" name="photo">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group" id="photo-preview2">
                            <label class="control-label col-md-3">Photo 2</label>
                            <div class="col-md-9">
                                (No photo)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" id="label-photo2">Photo 2</label>
                            <div class="col-md-9">
                                <input type="file" name="photo2">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group" id="photo-preview3">
                            <label class="control-label col-md-3">Photo 3</label>
                            <div class="col-md-9">
                                (No photo)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" id="label-photo3">Photo 3</label>
                            <div class="col-md-9">
                                <input type="file" name="photo3">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


