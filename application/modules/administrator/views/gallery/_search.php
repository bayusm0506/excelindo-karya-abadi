<div class="box box-default" id="close-search" style="display: none;">
  <div class="box-header with-border">
    <h3 class="box-title">Custom Filter : </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="form-filter" class="form-horizontal">
      <div class="col-lg-4">
        <label for="">Judul Foto</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <input class="typeahead form-control" placeholder="Judul Foto" type="text" id="jdl_foto" />          
        </div>
      </div>
      <div class="col-lg-4">
        <label for="status">Nama Album</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-check fa-lg" aria-hidden="true"></i>
            </div>
            <select class="select2 form-control" id="id_album_s" style="width:100%;">
              <option value="">Pilih Album</option>
              <?php
                if ($album->num_rows() > 0)
                {
                   foreach ($album->result() as $row)
                   {
              ?>
                <option value="<?=$row->id_album?>"><?=$row->jdl_album?></option>    
              <?php
                   }
                }
              ?>
           </select>
        </div>
      </div>
      <div class="col-lg-12">
        <label for="filter"></label>        
        <div class="input-group">
            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
        </div>
      </div>
    </form>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <!-- box-footer -->
  </div>
</div>