<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Daftar
    <small>Lowongan</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/lowker">Modul Web</a></li>
    <li class="active">Lowongan Kerja</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<!-- SELECT2 EXAMPLE -->
<?php $this->load->view('lowker/_search'); ?>
<?php $this->load->view('lowker/_form'); ?>
<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Data Lowongan Kerja</h3>
    <div class="box-tools pull-right">
      <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
      <button type="button" class="btn bg-maroon btn-flat" onclick="add_lowker()">
          <i class="fa fa-plus"></i> Tambah Data
      </button>
      <button class="btn bg-olive btn-flat" onclick="custom_filter()">
          <i class="fa fa-gear"></i> <span id="custom"></span>
      </button>
      <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
    </div>
  </div>
<!-- /.box-header -->
  <?php $this->load->view('lowker/_dataTable'); ?>
</div>
<!-- /.box -->
</section>
<!-- /.content -->

<script type="text/javascript">
  var save_method; //for save method string
  var table;
  var base_url = '<?php echo base_url();?>';
  $("#custom").text('Filter');
  $( "#judul" ).autocomplete({
    source: "<?php echo site_url('administrator/lowker/get_autocomplete_judul/?');?>"
  });

  $( "#nama_perusahaan" ).autocomplete({
    source: "<?php echo site_url('administrator/lowker/get_autocomplete_nama_perusahaan/?');?>"
  });

  function custom_filter() {
      var x = document.getElementById("close-search");
      if (x.style.display === "block") {
          x.style.display = "none";
          $("#custom").text('Filter');
      } else {
          x.style.display = "block";
          $("#custom").text('Close Filter');
      }
  }

  $(document).ready(function() {
      //datatables
      table = $('#table').DataTable({ 
          language: {
              //search: "_INPUT_",
              searchPlaceholder: "Cari Disini..."
          },
          dom: 'Blfrtip',
          buttons: [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ],
          "pagingType": "full_numbers",
          "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?php echo site_url('administrator/lowker/lowker_ajax_list')?>",
              "type": "POST",
              "data": function ( data ) {
                  data.judul = $('#judul').val();
                  data.nama_perusahaan = $('#nama_perusahaan').val();
                  data.deadline = $('#tanggal_deadline').val();
              }
          },

          //Set column definition initialisation properties.
          "columnDefs": [
            { 
                "targets": [ 0,-1 ], //last column
                "orderable": false, //set not orderable
            },
          ],

      });

      //datepicker
      $('.datepicker').datepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
          todayHighlight: true,
          //orientation: "top auto",
          todayBtn: true,
          todayHighlight: true,  
      });
      //var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
      //$('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("input[type=radio]").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
  });

  $('#btn-filter').click(function(){ //button filter event click
      table.ajax.reload();  //just reload table
  });
  
  $('#btn-reset').click(function(){ //button reset event click
      $('#form-filter')[0].reset();
      table.ajax.reload();  //just reload table
  });

  function add_lowker()
  {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Lowongan Kerja'); // Set Title to Bootstrap modal title

      $('#pendukung-preview').hide(); // hide pendukung preview modal

      $('#label-pendukung').text('Upload File'); // label pendukung upload

      $('#deskripsi-preview div').html('<textarea name="deskripsi" id="deskripsi"></textarea>'); // remove photo
      CKEDITOR.replace( 'deskripsi' );

      $('#keterangan-preview div').html('<textarea name="keterangan" id="keterangan"></textarea>'); // remove photo
      CKEDITOR.replace( 'keterangan' );
  }

  

  function reload_table()
  {
      table.ajax.reload(null,false); //reload datatable ajax 
  }

  

  function delete_lowker(id)
  {
    swal({
      title: "Are you sure?",
      text: "You will delete this Record!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url('administrator/lowker/delete')?>',
          data: 'empid='+id
        });
        reload_table();
        swal("Deleted!", "Record has been deleted.", "success");

      } else {
        swal("Cancelled", "Your Record is safe :)", "error");
      }
    });
  }
</script>

<?php //$this->load->view('lowker/_form'); ?>

<script type="text/javascript">

  function edit_lowker(id)
  {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string


      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo site_url('administrator/lowker/ajax_edit')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
              $('.modal-title').text('Edit Lowongan Kerja'); // Set title to Bootstrap modal title

              $('[name="id"]').val(data.id_lowongan);
              $('[name="judul"]').val(data.judul);
              $('[name="nama_perusahaan"]').val(data.nama_perusahaan);
              $('[name="posisi"]').val(data.posisi);
              $('[name="deadline"]').datepicker('update',data.deadline);
              $('[name="keterangan"]').val(data.keterangan);
             
              $('#pendukung-preview').show(); // show pendukung preview modal
              if (data.deskripsi_perusahaan) {
                $('#deskripsi-preview div').html('<textarea name="deskripsi" id="deskripsi">'+data.deskripsi_perusahaan+'</textarea>'); 
                CKEDITOR.replace( 'deskripsi' );
              }else{
                $('#deskripsi-preview div').html('<textarea name="deskripsi" id="deskripsi"></textarea>'); 
                CKEDITOR.replace( 'deskripsi' );
              }

              if (data.keterangan) {
                $('#keterangan-preview div').html('<textarea name="keterangan" id="keterangan">'+data.keterangan+'</textarea>'); 
                CKEDITOR.replace( 'keterangan' );
              }else{
                $('#keterangan-preview div').html('<textarea name="keterangan" id="keterangan"></textarea>'); 
                CKEDITOR.replace( 'keterangan' );
              }


              if(data.file_pendukung)
              {
                  $('#label-pendukung').text('Change File'); // label File upload
                  $('#pendukung-preview div').html('<a href="'+base_url+'asset/files/'+data.file_pendukung+'">File Pendukung</a><br />'); // show photo
                  $('#pendukung-preview div').append('<input type="checkbox" name="remove_file" value="'+data.file_pendukung+'"/> Remove File when saving'); // remove photo

              }else
              {
                  $('#label-pendukung').text('Upload File'); // label File upload
                  $('#pendukung-preview div').text('(No File)');
              }


          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

  function save()
  {
      $('#btnSave').text('saving...'); //change button text
      $('#btnSave').attr('disabled',true); //set button disable 
      CKEDITOR.instances.deskripsi.updateElement();
      CKEDITOR.instances.keterangan.updateElement();
      var url;

      if(save_method == 'add') {
          url = "<?php echo site_url('administrator/lowker/save')?>";
      } else {
          url = "<?php echo site_url('administrator/lowker/edit')?>";
      }

      // ajax adding data to database

      var formData = new FormData($('#form')[0]);
      $.ajax({
          url : url,
          type: "POST",
          data: formData,
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {

              if(data.status) //if success close modal and reload ajax table
              {
                  $('#modal_form').modal('hide');
                  reload_table();
                  //Ajax.show_alert('info',data.msg);
                  $.notify(data.msg,"success");
              }
              else
              {
                  for (var i = 0; i < data.inputerror.length; i++) 
                  {
                      $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                      $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                  }
              }
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 


          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 
          }
      });
  }
</script>