<div class="box box-default" id="close-search" style="display: none;">
  <div class="box-header with-border">
    <h3 class="box-title">Custom Filter : </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="form-filter" class="form-horizontal">
      <div class="col-lg-4">
        <label for="status">Level Group Menu</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-check fa-lg" aria-hidden="true"></i>
            </div>
            <select class="select2 form-control" style="width:100%;" id="level_menu_s">
              <option value="">Pilih Group Menu</option>
              <?php
                if ($menu_group->num_rows() > 0)
                {
                   foreach ($menu_group->result() as $row)
                   {
              ?>
                <option value="<?=$row->id_group_menu?>"><?=$row->nama_group_menu?></option>    
              <?php
                   }
                }
              ?>
           </select>
        </div>
      </div>
      <div class="col-lg-4">
        <label for="nama_group_menu">Nama Group Menu</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Nama Group Menu" type="text" id="nama" />          
        </div>
      </div>
      <div class="col-lg-4">
        <label for="link_menu">Nama Group Menu</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Link / URL" type="text" id="link_menu" />          
        </div>
      </div>
      <div class="col-lg-12">
        <label for="filter"></label>        
        <div class="input-group">
            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
        </div>
      </div>
    </form>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <!-- box-footer -->
  </div>
</div>