<div class="box-body">
  <table id="table" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>No</th>
      <th>Menu</th>
      <th>Level Menu</th>
      <th>Link</th>
      <th>Aktif</th>
      <th>Position</th>
      <th>Urutan</th>
      <th>Bahasa</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      
    </tbody>
    <tfoot>
    <tr>
      <th>No</th>
      <th>Menu</th>
      <th>Level Menu</th>
      <th>Link</th>
      <th>Aktif</th>
      <th>Position</th>
      <th>Urutan</th>
      <th>Bahasa</th>
      <th>Action</th>
    </tr>
    </tfoot>
  </table>
</div>
<!-- /.box-body -->