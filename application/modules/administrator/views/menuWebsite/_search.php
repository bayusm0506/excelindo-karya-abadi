<div class="box box-default" id="close-search" style="display: none;">
  <div class="box-header with-border">
    <h3 class="box-title">Custom Filter : </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="form-filter" class="form-horizontal">
      <div class="col-lg-4">
        <label for="">Nama Menu</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <input class="typeahead form-control" placeholder="Nama Menu" type="text" id="nama_menu" />          
        </div>
      </div>
      <div class="col-lg-4">
        <label for="status">Level Menu</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-check fa-lg" aria-hidden="true"></i>
            </div>
            <select class="select2 form-control" id="level_menu_s" style="width:100%;">
              <option value="">Pilih Level Menu</option>
              <option value="0">Menu Utama</option>
              <?php
                if ($menu_utama->num_rows() > 0)
                {
                   foreach ($menu_utama->result() as $row)
                   {
              ?>
                <option value="<?=$row->id_menu?>"><?=$row->nama_menu?></option>    
              <?php
                   }
                }
              ?>
           </select>
        </div>
      </div>
      <div class="col-lg-4">
        <label for="">Link Menu</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Link Menu" type="text" id="link_menu" />          
        </div>
      </div>
      <div class="col-lg-4">
          <label for="status">Status</label>        
          <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa fa-check fa-lg" aria-hidden="true"></i>
              </div>
              <select id="status_s" class="select2 form-control" style="width:100%;">
                <option value="">Pilih Status</option>
                <option value="Ya">Aktif</option>
                <option value="Tidak">Tidak Aktif</option>
              </select>
          </div>
        </div>
        <div class="col-lg-4">
          <label for="status">Position</label>        
          <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa fa-check fa-lg" aria-hidden="true"></i>
              </div>
              <select id="position_s" class="select2 form-control" style="width:100%;">
                <option value="">Pilih Potition</option>
                <option value="Top">Top</option>
                <option value="Bottom">Bottom</option>
              </select>
          </div>
      </div>  
      <div class="col-lg-4">
        <label for="SubjekPajak_subjek_pajak_id_pencarian">Urutan</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Urutan" type="number" id="urutan" />          
        </div>
      </div>
      <div class="col-lg-12">
        <label for="filter"></label>        
        <div class="input-group">
            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
        </div>
      </div>
    </form>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <!-- box-footer -->
  </div>
</div>