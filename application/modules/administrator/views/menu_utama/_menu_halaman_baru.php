<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    List
    <small>Berita</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/modul_berita/index">Modul Berita</a></li>
    <li class="active">List Berita</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <!-- SELECT2 EXAMPLE -->
<div class="box box-default" id="close-search" style="display: none;">
  <div class="box-header with-border">
    <h3 class="box-title">Custom Filter : </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="form-filter" class="form-horizontal">
      <div class="col-lg-4">
        <label for="judul">Judul</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Judul Berita" type="text" id="judul" />          
        </div>
      </div>
      <div class="col-lg-4">
        <label for="judul">Tanggal Posting</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Tanggal Posting" type="text" id="tanggal" />          
            <script type="text/javascript">
              //Date picker
              $('#tanggal').datepicker({
                changeMonth: true,
                changeYear: true,
                autoclose: true,
                dateFormat: 'yy-mm-dd'
              })
                // $( function() {
                //   $( "#tanggal_posting" ).datepicker({
                //     changeMonth: true,
                //     changeYear: true
                //   });
                // } );
            </script>
        </div>
      </div>
      <div class="col-lg-4">
          <label for="status">Status</label>        
          <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa fa-check fa-lg" aria-hidden="true"></i>
              </div>
              <select id="status" class="form-control">
                <option value="">Pilih Status</option>
                <option value="Y">Published</option>
                <option value="N">Unpublished</option>
              </select>
          </div>
        </div>
      <div class="col-lg-6">
        <label for="filter"></label>        
        <div class="input-group">
            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
        </div>
      </div>
    </form>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <!-- box-footer -->
  </div>
</div>
<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
<div class="box-header with-border">
  <h3 class="box-title">Data List Berita</h3>
  <div class="box-tools pull-right">
    <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-add">
        <i class="fa fa-plus"></i> Tambah Data
    </button>
    <button class="btn btn-danger" onclick="Close_Search()">
        <i class="fa fa-gear"></i> Custom Filter
    </button>
    <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
  </div>
</div>
<!-- /.box-header -->
<div class="box-body">
  <table id="table" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>No</th>
      <th>Judul Berita</th>
      <th>Tanggal</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      
    </tbody>
    <tfoot>
    <tr>
      <th>No</th>
      <th>Judul Berita</th>
      <th>Tanggal</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
    </tfoot>
  </table>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</section>
<!-- /.content -->

<!-- MODAL ADD -->
<div class="modal fade" id="modal-add">
  <div class="modal-dialog" style="width:950px;">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Form Tambah List Berita</h3>

        <div class="box-tools pull-right">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-window-close" style="font-size:32px" aria-hidden="true"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <?php echo form_open_multipart('administrator/modul_berita/saveListBerita', array('id'=>'form-list-berita', 'role'=>'form', 'class'=>'form-horizontal', 'name'=>'form-list-berita'));?>
        <div class="box-body">
          <div class="form-group">
            <label for="judul" class="col-sm-2 control-label">Judul</label>
            <div class="col-sm-10">
              <?php echo form_input('judul', '', array('class' => 'form-control', 'placeholder' => 'Judul', 'id' => 'judul'));?>
            </div>
          </div>
          <div class="form-group">
            <label for="sub_judul" class="col-sm-2 control-label">Sub Judul</label>

            <div class="col-sm-10">
              <?php echo form_input('sub_judul', '', array('class' => 'form-control', 'placeholder' => 'Sub Judul', 'id' => 'sub_judul'));?>
            </div>
          </div>
          <div class="form-group">
            <label for="video" class="col-sm-2 control-label">Video Youtube</label>

            <div class="col-sm-10">
              <?php echo form_input('video', '', array('class' => 'form-control', 'placeholder' => 'Contoh link: http://www.youtube.com/embed/xbuEmoRWQHU', 'id' => 'video'));?>
            </div>
          </div>
          <div class="form-group">
            <label for="kategori_berita" class="col-sm-2 control-label">Kategori</label>
            <div class="col-sm-10">
              <select class="select2 form-control" name="id_kategori_berita" style="width:100%;">
                  <option value="">Pilih Kategori Berita</option>
                  <?php
                    foreach ($kategori_berita as $row){
                        echo "<option value='$row[id_kategori]'>$row[nama_kategori]</option>";
                    }
                  ?>
               </select>
            </div>
          </div>
          <div class="form-group">
            <label for="headline" class="col-sm-2 control-label">Headline</label>

            <div class="col-sm-10">
                <input type="radio" name="headline" class="flat-red" value="Y">Ya
                <input type="radio" name="headline" class="flat-red" checked value="N">Tidak
            </div>
          </div>
          <div class="form-group">
            <label for="pilihan" class="col-sm-2 control-label">Pilihan</label>

            <div class="col-sm-10">
                <input type="radio" name="pilihan" class="flat-red" value="Y">Ya
                <input type="radio" name="pilihan" class="flat-red" checked value="N">Tidak
            </div>
          </div>
          <div class="form-group">
            <label for="berita_utama" class="col-sm-2 control-label">Berita Utama</label>

            <div class="col-sm-10">
                <input type="radio" name="berita_utama" class="flat-red" value="Y">Ya
                <input type="radio" name="berita_utama" class="flat-red" checked value="N">Tidak
            </div>
          </div>
          <div class="form-group">
            <label for="isi_berita" class="col-sm-2 control-label">Isi Berita</label>
            <div class="col-sm-10">
              <textarea name="isi_berita" id='isi_berita' class='form-control' placeholder="Isi Berita" style="height:260px"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="gambar" class="col-sm-2 control-label">Gambar</label>
            <div class="col-sm-10">
              <input type="file" name="gambar" id="imgInp"><br />
              <img id="bsm" src="#" alt="Preview in Here"/>
            </div>
          </div>
          <div class="form-group">
            <label for="keterangan_gambar" class="col-sm-2 control-label">Keterangan Gambar</label>

            <div class="col-sm-10">
              <?php echo form_input('keterangan_gambar', '', array('class' => 'form-control', 'placeholder' => 'Keterangan Gambar', 'id' => 'keterangan_gambar'));?>
            </div>
          </div>
          <div class="form-group">
            <label for="tag" class="col-sm-2 control-label">Tag</label>

            <div class="col-sm-10">
             <?php
                foreach ($tag as $tag){
                    echo "<span style='display:inline-block;'><input type=checkbox class='minimal-red' value='$tag[tag_seo]' name=tag[]>$tag[nama_tag] &nbsp; &nbsp; &nbsp; </span>";
                }
             ?>
            </div>
          </div>
          <div id="element-progress" style="width:100px;" class="pull-left"></div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-info pull-right">Simpan</button>
        </div>
        <!-- /.box-footer -->
      <?php echo form_close();?>
      <!-- /.box-body -->
      <!-- <div class="box-footer">
        Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
        the plugin.
      </div> -->
    </div>
  </div>
</div>
<!-- END MODAL ADD -->
<?php 
    foreach($record as $i):
?>
<!-- MODAL UPDATE -->
<div class="modal fade" id="modal_edit<?=$i['id_berita']?>">
  <div class="modal-dialog" style="width:950px;">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Form Edit List Berita</h3>

        <div class="box-tools pull-right">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-window-close" style="font-size:32px" aria-hidden="true"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <?php echo form_open_multipart('administrator/modul_berita/saveListBerita', array('id'=>'form-list-berita-edit', 'role'=>'form', 'class'=>'form-horizontal'));?>
        <div class="box-body">
          <div class="form-group">
            <label for="judul" class="col-sm-2 control-label">Judul</label>
            <div class="col-sm-10">
              <input type="hidden" name="id_edit" value="<?=$i['id_berita']?>">
              <?php echo form_input('judul', $i['judul'], array('class' => 'form-control', 'placeholder' => 'Judul', 'id' => 'judul'));?>
            </div>
          </div>
          <div class="form-group">
            <label for="sub_judul" class="col-sm-2 control-label">Sub Judul</label>

            <div class="col-sm-10">
              <?php echo form_input('sub_judul', $i['sub_judul'], array('class' => 'form-control', 'placeholder' => 'Sub Judul', 'id' => 'sub_judul'));?>
            </div>
          </div>
          <div class="form-group">
            <label for="video" class="col-sm-2 control-label">Video Youtube</label>

            <div class="col-sm-10">
              <?php echo form_input('video', $i['youtube'], array('class' => 'form-control', 'placeholder' => 'Contoh link: http://www.youtube.com/embed/xbuEmoRWQHU', 'id' => 'video'));?>
            </div>
          </div>
          <div class="form-group">
            <label for="kategori_berita" class="col-sm-2 control-label">Kategori</label>
            <div class="col-sm-10">
              <select class="select2 form-control" name="id_kategori_berita" style="width:100%;">
                  <option value="">Pilih Kategori Berita</option>
                  <?php
                    foreach ($kategori_berita as $row){
                        echo "<option value='$row[id_kategori]'>$row[nama_kategori]</option>";
                    }
                  ?>
               </select>
            </div>
          </div>
          <div class="form-group">
            <label for="headline" class="col-sm-2 control-label">Headline</label>

            <div class="col-sm-10">
                <input type="radio" name="headline" class="flat-red" value="Y">Ya
                <input type="radio" name="headline" class="flat-red" checked value="N">Tidak
            </div>
          </div>
          <div class="form-group">
            <label for="pilihan" class="col-sm-2 control-label">Pilihan</label>

            <div class="col-sm-10">
                <input type="radio" name="pilihan" class="flat-red" value="Y">Ya
                <input type="radio" name="pilihan" class="flat-red" checked value="N">Tidak
            </div>
          </div>
          <div class="form-group">
            <label for="berita_utama" class="col-sm-2 control-label">Berita Utama</label>

            <div class="col-sm-10">
                <input type="radio" name="berita_utama" class="flat-red" value="Y">Ya
                <input type="radio" name="berita_utama" class="flat-red" checked value="N">Tidak
            </div>
          </div>
          <div class="form-group">
            <label for="isi_berita" class="col-sm-2 control-label">Isi Berita</label>
            <div class="col-sm-10">
              <textarea name="isi_berita" id='isi_berita<?=$i['id_berita']?>' class='form-control' placeholder="Isi Berita" style="height:260px"><?=$i['id_berita']?></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="gambar" class="col-sm-2 control-label">Gambar</label>
            <div class="col-sm-10">
              <input type="file" name="gambar" id="exampleInputFile">
              <img id="bsm_update" src="#" alt="Preview in Here"/>
              <?php
                if (!empty($i['gambar'])) {
                  echo "<img src='".base_url()."asset/foto_berita/$i[gambar]'>";
                }else{
                  echo "Gambar Masih Kosong";
                }
              ?>
            </div>
          </div>
          <div class="form-group">
            <label for="keterangan_gambar" class="col-sm-2 control-label">Keterangan Gambar</label>

            <div class="col-sm-10">
              <?php echo form_input('keterangan_gambar', '', array('class' => 'form-control', 'placeholder' => 'Keterangan Gambar', 'id' => 'keterangan_gambar'));?>
            </div>
          </div>
          <div class="form-group">
            <label for="tag" class="col-sm-2 control-label">Tag</label>

            <div class="col-sm-10">
             <?php
                foreach ($tag_edit as $tag){
                    echo "<span style='display:inline-block;'><input type=checkbox class='minimal-red' value='$tag[tag_seo]' name=tag[]>$tag[nama_tag] &nbsp; &nbsp; &nbsp; </span>";
                }
             ?>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-info pull-right">Simpan</button>
        </div>
        <!-- /.box-footer -->
      <?php echo form_close();?>
      <!-- /.box-body -->
      <!-- <div class="box-footer">
        Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
        the plugin.
      </div> -->
    </div>
  </div>
</div>
<script type="text/javascript">
  CKEDITOR.replace('isi_berita<?=$i['id_berita']?>' ,{
    filebrowserImageBrowseUrl : '<?php echo base_url(); ?>asset/kcfinder'
  });
  $('form#form-list-berita-edit').submit(function(){
      CKEDITOR.instances.isi_berita<?=$i['id_berita']?>.updateElement();
      Ajax.upload( "<?=site_url('administrator/modul_berita/editListBerita')?>",'form-list-berita-edit',function(response){
        if( response.status == 'info'){
          $('#modal_edit<?=$i['id_berita']?>').modal('hide');
          reload_table();
          Ajax.show_alert('info',response.msg);
          //setInterval(function(){ window.location.href = response.url; }, 3000);
        }else{
          Ajax.show_alert('error',response.msg);
        }
        Ajax.progressFull();
      });
      return false;
    });
</script>
<!-- END MODAL UPDATE -->
<?php endforeach;?>
<!-- /.box -->
<script type="text/javascript">
  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#bsm').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURLUpdate(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#bsm_update').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    readURL(this);
  });

  $("#imgInp_update").change(function() {
    readURLUpdate(this);
  });
</script>
<script type="text/javascript">
  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#bsm').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURLUpdate(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#bsm_update').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    readURL(this);
  });

  $("#imgInp_update").change(function() {
    readURLUpdate(this);
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
      $( "#judul" ).autocomplete({
        source: "<?php echo site_url('administrator/modul_berita/get_autocomplete_listBerita/?');?>"
      });
  });

  function Close_Search() {
      var x = document.getElementById("close-search");
      if (x.style.display === "block") {
          x.style.display = "none";
      } else {
          x.style.display = "block";
      }
  }
</script>
<script type="text/javascript">
  var table;
  $(document).ready(function() {

      //datatables
      table = $('#table').DataTable({ 
          language: {
              //search: "_INPUT_",
              searchPlaceholder: "Cari Disini..."
          },
          dom: 'Blfrtip',
          buttons: [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ],
          "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?php echo site_url('administrator/modul_berita/listBerita_ajax_list')?>",
              "type": "POST",
              "data": function ( data ) {
                  data.judul = $('#judul').val();
                  data.tanggal = $('#tanggal').val();
                  data.status = $('#status').val();
              }
          },

          //Set column definition initialisation properties.
          "columnDefs": [
            { 
                "targets": [ 0,-1 ], //last column
                "orderable": false, //set not orderable
            },
          ],

      });
      var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
      $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
  });

  $('#btn-filter').click(function(){ //button filter event click
      table.ajax.reload();  //just reload table
  });
  
  $('#btn-reset').click(function(){ //button reset event click
      $('#form-filter')[0].reset();
      table.ajax.reload();  //just reload table
  });

  function reload_table()
  {
      table.ajax.reload(null,false); //reload datatable ajax 
  }

  function delete_menugroup(id)
  {
    swal({
      title: "Are you sure?",
      text: "You will delete this Record!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url('administrator/modul_berita/deleteListBerita')?>',
          data: 'empid='+id
        });
        reload_table();
        swal("Deleted!", "Record has been deleted.", "success");

      } else {
        swal("Cancelled", "Your Record is safe :)", "error");
      }
    });
  }

  CKEDITOR.replace('isi_berita' ,{
    filebrowserImageBrowseUrl : '<?php echo base_url(); ?>asset/kcfinder'
  });
  
  $('form#form-list-berita').submit(function(){
    CKEDITOR.instances.isi_berita.updateElement();
    Ajax.upload( "<?=site_url('administrator/modul_berita/saveListBerita')?>",'form-list-berita',function(response){
      if( response.status == 'ok'){
        Ajax.show_alert('info',response.msg);
        $('form#form-list-berita')[0].reset(); // reset form on modals
        $('#modal-add').modal('hide');
        reload_table();
      }else{
        Ajax.show_alert('error',response.msg);
      }
      Ajax.progressFull();
    });
    return false;
  });
</script>