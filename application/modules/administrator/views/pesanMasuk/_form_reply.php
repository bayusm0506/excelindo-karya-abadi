<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_reply" role="dialog">
    <div class="modal-dialog" style="width:950px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Pesan Masuk Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Pengirim</label>
                            <div class="col-md-9">
                                <input name="nama" placeholder="Nama Pengirim" class="form-control" type="text"  disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email Pengirim</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="Email Pengirim" class="form-control" type="text"  disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Subjek Pesan</label>
                            <div class="col-md-9">
                                <input name="subjek" placeholder="Subjek Pesan" class="form-control" type="text"  disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group" id="isi-preview-reply">
                            <label class="control-label col-md-3">Isi Pesan</label>
                            <div class="col-md-9">

                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="balas-preview">
                            <label class="control-label col-md-3">Balas Pesan</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="balas_pesan" style="height:120px;"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


