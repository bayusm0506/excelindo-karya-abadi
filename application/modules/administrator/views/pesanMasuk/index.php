<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pesan
    <small>Masuk</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=base_url()?>administrator/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=base_url()?>administrator/pesanMasuk">Modul Web</a></li>
    <li class="active">Pesan Masuk</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <?php $this->load->view('pesanMasuk/_search'); ?>
  <!-- /.box -->
  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Data Pesan Masuk</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
        <button type="button" class="btn bg-maroon btn-flat" onclick="add_pm()">
            <i class="fa fa-plus"></i> Tambah Data
        </button>
        <button class="btn bg-olive btn-flat" onclick="custom_filter()">
            <i class="fa fa-gear"></i> <span id="custom"></span>
        </button>
        
        <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
      </div>
    </div>
    <!-- /.box-header -->
    <?php $this->load->view('pesanMasuk/_dataTable'); ?>
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
<script type="text/javascript">
  var save_method; //for save method string
  var table;
  var base_url = '<?php echo base_url();?>';
  $("#custom").text('Filter');
  $( "#nama" ).autocomplete({
    source: "<?php echo site_url('administrator/pesanMasuk/get_autocomplete_nama/?');?>"
  });

  $( "#email" ).autocomplete({
    source: "<?php echo site_url('administrator/pesanMasuk/get_autocomplete_email/?');?>"
  });

  function custom_filter() {
      var x = document.getElementById("close-search");
      if (x.style.display === "block") {
          x.style.display = "none";
          $("#custom").text('Filter');
      } else {
          x.style.display = "block";
          $("#custom").text('Close Filter');
      }
  }
  
  $(document).ready(function() {
      //datatables
      table = $('#table').DataTable({ 
          language: {
              //search: "_INPUT_",
              searchPlaceholder: "Cari Disini..."
          },
          dom: 'Blfrtip',
          buttons: [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ],
          "pagingType": "full_numbers",
          "lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
          //"buttons": ['csv','print', 'excel', 'pdf'],
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?php echo site_url('administrator/pesanMasuk/ajax_list')?>",
              "type": "POST",
              "data": function ( data ) {
                  data.nama = $('#nama').val();
                  data.email = $('#email').val();
                  data.tanggal = $('#tanggal_s').val();
                  data.dibaca = $('#status').val();
              }
          },

          //Set column definition initialisation properties.
          "columnDefs": [
            { 
                "targets": [ 0,-1 ], //last column
                "orderable": false, //set not orderable
            },
          ],

      });
      // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
      // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
      //set input/textarea/select event when change value, remove class error and remove text help block 
      $("input").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
      $("textarea").change(function(){
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
      });
  });

  $('#btn-filter').click(function(){ //button filter event click
      table.ajax.reload();  //just reload table
  });
  
  $('#btn-reset').click(function(){ //button reset event click
      $('#form-filter')[0].reset();
      $("select#status").val('').trigger('change');
      table.ajax.reload();  //just reload table
  });

  function add_pm()
  {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Link Terkait'); // Set Title to Bootstrap modal title
      $('#isi-preview div').html('<textarea name="isi_pesan" id="isi_pesan"></textarea>');
      CKEDITOR.replace( 'isi_pesan' );
  }

  function edit_pm(id)
  {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string


      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo site_url('administrator/pesanMasuk/ajax_edit')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

              $('[name="id"]').val(data.id_hubungi);
              $('[name="nama"]').val(data.nama);
              $('[name="email"]').val(data.email);
              $('[name="subjek"]').val(data.subjek);
              $('[name="pesan"]').val(data.pesan);
              $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
              $('.modal-title').text('Edit Pesan Masuk'); // Set title to Bootstrap modal title
              
              if (data.pesan) {
                $('#isi-preview div').html('<textarea name="isi_pesan" id="isi_pesan">'+data.pesan+'</textarea>'); 
                CKEDITOR.replace( 'isi_pesan' );
              }else{
                $('#isi-preview div').html('<textarea name="isi_pesan" id="isi_pesan"></textarea>'); 
                CKEDITOR.replace( 'isi_pesan' );
              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

  function balas_pm(id)
  {
      save_method = 'reply';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string


      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo site_url('administrator/pesanMasuk/ajax_edit')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

              $('[name="id"]').val(data.id_hubungi);
              $('[name="nama"]').val(data.nama);
              $('[name="email"]').val(data.email);
              $('[name="subjek"]').val(data.subjek);
              $('[name="pesan"]').val(data.pesan);
              $('#modal_form_reply').modal('show'); // show bootstrap modal when complete loaded
              $('.modal-title').text('Balas Pesan Masuk'); // Set title to Bootstrap modal title
              
              if (data.pesan) {
                $('#isi-preview-reply div').html('<textarea name="isi_pesan" id="isi_pesan" disabled>'+data.pesan+'</textarea>'); 
                CKEDITOR.replace( 'isi_pesan' );
              }else{
                $('#isi-preview-reply div').html('<textarea name="isi_pesan" id="isi_pesan"></textarea>'); 
                CKEDITOR.replace( 'isi_pesan' );
              }

              // $('#balas-preview div').html('<textarea name="balas_pesan" id="balas_pesan"></textarea>'); 
              // CKEDITOR.replace( 'balas_pesan' );
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

  function reload_table()
  {
      table.ajax.reload(null,false); //reload datatable ajax 
  }

  function save()
  {
      $('#btnSave').text('saving...'); //change button text
      $('#btnSave').attr('disabled',true); //set button disable 
      CKEDITOR.instances.isi_pesan.updateElement();
      var url;

      if(save_method == 'add') {
          url = "<?php echo site_url('administrator/pesanMasuk/save')?>";
      } else if(save_method == 'update') {
          url = "<?php echo site_url('administrator/pesanMasuk/edit')?>";
      }else{
          // CKEDITOR.instances.balas_pesan.updateElement();
          url = "<?php echo site_url('administrator/pesanMasuk/reply')?>";
      }

      // ajax adding data to database

      var formData = new FormData($('#form')[0]);
      $.ajax({
          url : url,
          type: "POST",
          data: formData,
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {

              if(data.status) //if success close modal and reload ajax table
              {
                  $('#modal_form').modal('hide');
                  $('#modal_form_reply').modal('hide');
                  reload_table();
                  //Ajax.show_alert('info',data.msg);
                  $.notify(data.msg,"success");
              }
              else
              {
                  for (var i = 0; i < data.inputerror.length; i++) 
                  {
                      $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                      $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                  }
              }
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 


          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 
          }
      });
  }

  function delete_pm(id)
  {
    swal({
      title: "Are you sure?",
      text: "You will delete this Record!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url('administrator/pesanMasuk/delete')?>',
          data: 'empid='+id
        });
        reload_table();
        swal("Deleted!", "Record has been deleted.", "success");

      } else {
        swal("Cancelled", "Your Record is safe :)", "error");
      }
    });
  }
</script>

<?php $this->load->view('pesanMasuk/_form'); ?>
<?php $this->load->view('pesanMasuk/_form_reply'); ?>