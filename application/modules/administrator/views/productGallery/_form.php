<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Gallery Blog Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Judul / Keterangan</label>
                            <div class="col-md-9">
                                <input name="jdl_pg" placeholder="Judul / Keterangan" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="status-preview">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Berita Terkait</label>
                            <div class="col-md-9">
                                <select class="form-control" name="id_group" id="id_group_s">
                                  <option value="">Pilih Terkait</option>
                                  <?php
                                    if ($group->num_rows() > 0)
                                    {
                                       foreach ($group->result() as $row)
                                       {
                                  ?>
                                    <option value="<?=$row->id_group_menu?>"><?=$row->nama_group_menu?></option>    
                                  <?php
                                       }
                                    }
                                  ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="photo-preview">
                            <label class="control-label col-md-3">Photo</label>
                            <div class="col-md-9">
                                (No photo)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" id="label-photo">Upload Photo </label>
                            <div class="col-md-9">
                                <input type="file" name="photo">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="ket-preview">
                            <label class="control-label col-md-3">Keterangan</label>
                            <div class="col-md-9">
                                
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


