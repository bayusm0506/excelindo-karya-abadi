<div class="box-body">
  <table id="table" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>No</th>
      <th>Foto</th>
      <th>Nama</th>
      <th>Jabatan</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      
    </tbody>
    <tfoot>
    <tr>
      <th>No</th>
      <th>Foto</th>
      <th>Nama</th>
      <th>Jabatan</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
    </tfoot>
  </table>
</div>
<!-- /.box-body -->