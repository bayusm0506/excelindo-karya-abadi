<div class="box box-default" id="close-search" style="display: none;">
  <div class="box-header with-border">
    <h3 class="box-title">Custom Filter : </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="form-filter" class="form-horizontal">
      <div class="col-lg-4">
        <label for="">Name</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Nama" type="text" id="name" />          
        </div>
      </div>
      <div class="col-lg-4">
        <label for="">Status</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa fa-object-group fa-lg" aria-hidden="true"></i>
            </div>
            <select class="select2 form-control" id="verified" style="width: 100%;">
              <option value="">Pilih Status</option>
              <option value="Y">Sudah Diverifikasi</option>
              <option value="N">Belum Diverifikasi</option>
            </select>         
        </div>
      </div>
      <div class="col-lg-4">
        <label for="judul">Tanggal Posting</label>        
        <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
            </div>
            <input class="form-control" placeholder="Tanggal Posting" type="text" id="tanggal_posting" />          
            <script type="text/javascript">
              //Date picker
              $('#tanggal_posting').datepicker({
                changeMonth: true,
                changeYear: true,
                autoclose: true,
                dateFormat: 'yy-mm-dd'
              })
            </script>
        </div>
      </div>
      <div class="col-lg-6">
        <label for="filter"></label>        
        <div class="input-group">
            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
        </div>
      </div>
    </form>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <!-- box-footer -->
  </div>
</div>