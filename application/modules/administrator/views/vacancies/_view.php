<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_view" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_verified" class="form-horizontal" name="myForm">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama</label>
                            <div class="col-md-9">
                                <input name="name" placeholder="Nama" class="form-control" type="text" disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone</label>
                            <div class="col-md-9">
                                <input name="phone" placeholder="Phone" class="form-control" type="text" disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="Email" class="form-control" type="email" disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Position</label>
                            <div class="col-md-9">
                                <input name="position" placeholder="Position" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group" id="attach1-preview">
                            <label class="control-label col-md-3">Attachment</label>
                            <div class="col-md-9">
                                (No Attachment)
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group" id="attach2-preview">
                            <label class="control-label col-md-3">Attachment</label>
                            <div class="col-md-9">
                                (No Attachment)
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group" id="attach3-preview">
                            <label class="control-label col-md-3">Attachment</label>
                            <div class="col-md-9">
                                (No Attachment)
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group" id="attach4-preview">
                            <label class="control-label col-md-3">Attachment</label>
                            <div class="col-md-9">
                                (No Attachment)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select name="verified" class="form-control">
                                    <option value="N">Belum Diverifikasi</option>
                                    <option value="Y">Sudah Diverifikasi</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
