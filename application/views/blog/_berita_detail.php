<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="<?=site_url()?>">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="<?=site_url('blog/kategori/')?><?=$record['kategori_seo']?>"><?=ucwords($record['kategori_seo'])?></a> <i class="icon-angle-right"></i></li>
            <li class="active"><?=$record['judul']?></li>
          </ul>
          <h2>Blog Detail</h2>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="content">
  <div class="container">
    <div class="row">

      <div class="span8">

        <article class="single">
          <div class="row">

            <div class="span8">
              <div class="post-image">
                <div class="post-heading">
                  <h3><a href="#"><?=$record['judul']?></a></h3>
                </div>
                <img src="<?=base_url()?>asset/foto_berita/<?=$record['gambar']?>" alt="<?=$record['judul']?>" />
              </div>
              <div class="meta-post">
                <ul>
                  <li><i class="icon-file"></i></li>
                  <li>By <a href="#" class="author"><?=$record['first_name']?></a></li>
                  <li>On <a href="#" class="date"><?=tgl_indo($record['tanggal'])?></a></li>
                  <li>Tags: <a href="#" style="text-transform: uppercase;"><?=$record['tag']?></a></li>
                </ul>
              </div>
              <p>
                <?=$record['isi_berita']?>
              </p>
            </div>
          </div>
        </article>
        <div class="about-author">
        <style>
          div.gallery {
              margin: 5px;
              border: 1px solid #ccc;
              float: left;
              width: 170px;
          }

          div.gallery:hover {
              border: 1px solid #777;
          }

          div.gallery img {
              width: 100%;
              height: auto;
          }

          div.desc {
              padding: 15px;
              text-align: center;
          }
        </style>
        <?php 
          if ($jumGallery > 0) {
            foreach ($gallery as $p) {
        ?>
          <div class="gallery">
            <a href="<?=base_url()?>asset/blog_gallery/<?=$p['gbr_bg']?>" title="<?=$p['jdl_bg']?>" data-gallery>
              <img src="<?=base_url()?>asset/blog_gallery/<?=$p['gbr_bg']?>" alt="<?=$p['jdl_bg']?>">
            </a>
            <div class="desc"><?=$p['jdl_bg']?></div>
          </div>
        <?php 
            }
          } 
        ?>
        
        <div id="blueimp-gallery" class="blueimp-gallery">
          <div class="slides"></div>
          <h3 class="title"></h3>
          <a class="prev" style="color:#fff">‹</a>
          <a class="next" style="color:#fff">›</a>
          <a class="close" style="color:#fff">×</a>
          <a class="play-pause"></a>
          <ol class="indicator"></ol>
        </div>
        </div>
        <!-- author info -->
        <div class="about-author">
    	  <?php
    	  	$identity = $this->MenuUtama_model->identitas()->row_array();
            $logo = $this->LogoWebsite_model->logo()->result_array();
            foreach ($logo as $p) {
              echo "<a href='".base_url()."' class='thumbnail align-left'><img src='".base_url()."asset/logo/$p[logo]' alt='Logo PT. Abbas Bumi Perkasa'></a>";
            }
          ?>
          <h5><strong><a href="#"><?=$identity['nama_website']?></a></strong></h5>
          <p>
            <?=$identity['keterangan']?>
          </p>
        </div>

        
        	<!-- <div class="comment-area fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5"></div> -->
        	<!-- <div class="fb-comments" data-href="http://abbasbumiperkasa.com/" data-width="600" data-numposts="5"></div> -->
      		<div class="fb-comments" data-href="http://abbasbumiperkasa.com/blog" data-width="770" data-numposts="5"></div>
      </div>

      <?php include "_widget.php"; ?>

    </div>
  </div>
</section>