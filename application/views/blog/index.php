<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="<?=site_url('blog')?>">Home</a> <i class="icon-angle-right"></i></li>
            <li class="active">Blog</li>
          </ul>
          <h2><?=$title?></h2>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="content">
  <div class="container">
    <div class="row">
      <?php include "_widget.php"; ?>
      <div class="span8">
        <?php
          foreach ($utama->result_array() as $r){
          $tgl = tgl_indo($r['tanggal']); 
          $isi_berita =(strip_tags($r['isi_berita'])); 
          $isi = substr($isi_berita,0,155); 
          $isi = substr($isi_berita,0,strrpos($isi," ")); 
          if (trim($isi)==''){
            $isi_artikel = 'Maaf, Tidak Ada ditemukan Informasi Dalam bentuk Teks, Silahkan Melihat Detail Informasi..';
          }else{
            $isi_artikel = $isi.' ....';
          }
          echo "
            <article>
              <div class='row'>
                <div class='span8'>
                  <div class='post-image'>
                    <div class='post-heading'>
                      <h3><a href='".base_url()."blog/detail/$r[judul_seo]' title='$r[judul]'>$r[judul]</a></h3>
                    </div>";
                    if ($r['gambar'] == ''){
                      echo "";
                    }else{
                      echo "<img src='".base_url()."asset/foto_berita/$r[gambar]' alt='$r[judul]' />";
                    }
                  echo "
                  </div>
                  <div class='meta-post'>
                    <ul>
                      <li><i class='icon-file'></i></li>
                      <li>By <a href='#' class='author'>$r[first_name]</a></li>
                      <li>On <a href='#' class='date'>$tgl</a></li>
                      <li>Clock <a href='#'>$r[jam]</a></li>
                      <li>View <a href='#'>$r[dibaca]</a></li>
                    </ul>
                  </div>
                  <div class='post-entry'>
                    <p>
                      $isi_artikel
                    </p>
                    <a href='".base_url()."blog/detail/$r[judul_seo]' class='readmore'>Read more <i class='icon-angle-right'></i></a>
                  </div>
                </div>
              </div>
            </article>
          ";
        } ?>
        <div id="pagination">
          <?php echo $this->pagination->create_links(); ?>
        </div>
      </div>
  </div>
</section>