<section id="inner-headline">

  <div class="container">

    <div class="row">

      <div class="span12">

        <div class="inner-heading">

          <ul class="breadcrumb">

            <li><a href="index.html">Home</a> <i class="icon-angle-right"></i></li>

            <li class="active">Kontak Kami</li>

          </ul>

          <h2></h2>

        </div>

      </div>

    </div>

  </div>

</section>

<?php

  $identity = $this->MenuUtama_model->identitas()->row_array();

?>

<section id="content">

  <div style="margin-bottom: 50px;">

    <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?=$identity['maps']?>"></iframe>

  </div>

  <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.392467061749!2d98.76212331431074!3d3.49624319745544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x303139769cdd3321%3A0xc9591c01ed8ffa89!2sPT.+ABBAS+BUMI+PERKASA!5e0!3m2!1sen!2sid!4v1542604198326" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->

  <!-- <div id="google-map" data-latitude="3.496564" data-longitude="98.764258"></div> -->

  <div class="container">



    <div class="row">

      <div class="span8">

        <?php if(get_cookie('lang_is') === 'en'){

        ?>

        <h4>HOW TO CONTACT US</h4>

        <p>We really want our customer service as reliable as our products. If there are questions, feedback or complaints, contact us through the form below. We will try to answer it as soon as possible.</p>

        <div id="sendmessage">Your message has been sent, we will contact you immediately</div>

        <div id="errormessage"></div>

          <form action="" method="post" role="form" class="contactForm">

          <div class="row">

            <div class="span4 form-group field">

              <input type="text" name="nama" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />

              <div class="validation"></div>

            </div>



            <div class="span4 form-group">

              <input type="email" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />

              <div class="validation"></div>

            </div>



            <div class="span4 form-group field">

              <input type="text" name="telephone" id="telephone" placeholder="Telephone (Optional)" data-rule="telephone" data-msg="Please enter Telephone" />

              <div class="validation"></div>

            </div>



            <div class="span4 form-group">

              <input type="text" name="website" id="website" placeholder="Website (Optional)" data-rule="website" data-msg="Please enter a Website" />

              <div class="validation"></div>

            </div>

            <div class="span8 form-group">

              <input type="text" name="subjek" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />

              <div class="validation"></div>

            </div>

            <div class="span8 form-group">

              <textarea name="isi_pesan" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>

              <div class="g-recaptcha" data-sitekey="6Lfqk5AUAAAAAH6R51bU6x85N1vIzWYMcnZ_HQ7g" data-theme="light" data-type="image" ></div>              <div class="validation"></div>

              <div class="text-center">

                <button class="btn btn-theme btn-medium margintop10" type="submit">Send a message</button>

              </div>

            </div>

          </div>

        </form>

        <?php }else{ ?>

          <h4>CARA MENGHUBUNGI KAMI</h4>

          <p>Kami sangat menginginkan layanan konsumen kami sehandal produk kami. Jika ada pertanyaan, masukan ataupun keluhan, hubungi kami melalui formulir dibawah ini. Kami akan berusaha untuk menjawabnya sesegera mungkin.</p>
          <div id="sendmessage">Pesan Anda telah terkirim, kami akan segera menghubungi Anda kembali</div>

          <div id="errormessage"></div>

            <form action="" method="post" role="form" class="contactForm">

            <div class="row">

              <div class="span4 form-group field">

                <input type="text" name="nama" id="name" placeholder="Nama" data-rule="minlen:4" data-msg="Tolong masukkan minimal 4 karakter" />

                <div class="validation"></div>

              </div>



              <div class="span4 form-group">

                <input type="email" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Tolong Masukkan Alamat Email yang valid" />

                <div class="validation"></div>

              </div>



              <div class="span4 form-group field">

                <input type="text" name="telephone" id="telephone" placeholder="Telepon (Optional)" data-rule="telephone" data-msg="Tolong Masukkan No. Telepon" />

                <div class="validation"></div>

              </div>



              <div class="span4 form-group">

                <input type="text" name="website" id="website" placeholder="Website (Optional)" data-rule="website" data-msg="Tolong Masukkan Website" />

                <div class="validation"></div>

              </div>

              <div class="span8 form-group">

                <input type="text" name="subjek" id="subject" placeholder="Subjek" data-rule="minlen:4" data-msg="Tolong Masukkan minimal 8 karakter di kolom" />

                <div class="validation"></div>

              </div>

              <div class="span8 form-group">

                <textarea name="isi_pesan" rows="5" data-rule="required" data-msg="Tolong Tulis Sesuatu buat kita" placeholder="Pesan"></textarea>

                <div class="g-recaptcha" data-sitekey="6Lfqk5AUAAAAAH6R51bU6x85N1vIzWYMcnZ_HQ7g" data-theme="light" data-type="image" ></div>              <div class="validation"></div>

                <div class="text-center">

                  <button class="btn btn-theme btn-medium margintop10" type="submit">Send a message</button>

                </div>

              </div>

            </div>

          </form>

        <?php } ?>

      </div>

      <div class="span4">

        <div class="clearfix"></div>

        <aside class="right-sidebar">



          <div class="widget">

            <?php if(get_cookie('lang_is') === 'en'){ ?>

              <h5 class="widgetheading">Contact information<span></span></h5>

            <?php }else{ ?>

              <h5 class="widgetheading">Informasi Kontak<span></span></h5>

            <?php } ?>

            

            <?php

              $identity = $this->MenuUtama_model->identitas()->row_array();

            ?>

            <ul class="contact-info">

              <li>

                <?php if(get_cookie('lang_is') === 'en'){ ?>

                  <label>Address :</label> 

                <?php }else{ ?>

                  <label>Alamat :</label> 

                <?php } ?>                

                <?=$identity['address']?>

              </li>

              <li>

                <?php if(get_cookie('lang_is') === 'en'){ ?>

                  <label>Phone :</label> 

                <?php }else{ ?>

                  <label>No. HP :</label> 

                <?php } ?>

                <?=$identity['no_telp']?> / <a href="https://api.whatsapp.com/send?phone=<?=$identity['whatsapp']?>&amp;text=Halo Abbas Bumi Perkasa, saya tertarik menggunakan jasa Anda. Bisa bantu saya?">+<?=$identity['whatsapp']?></a>

              </li>

              <li>

                <label>Email : </label> 

                <?=$identity['email']?>

              </li>

            </ul>



          </div>

        </aside>

      </div>

    </div>

  </div>

</section>



<script type="text/javascript">

  jQuery(document).ready(function($) {

    "use strict";



    //Contact

    $('form.contactForm').submit(function() {

      var f = $(this).find('.form-group'),

        ferror = false,

        emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;



      f.children('input').each(function() { // run all inputs



        var i = $(this); // current input

        var rule = i.attr('data-rule');



        if (rule !== undefined) {

          var ierror = false; // error flag for current input

          var pos = rule.indexOf(':', 0);

          if (pos >= 0) {

            var exp = rule.substr(pos + 1, rule.length);

            rule = rule.substr(0, pos);

          } else {

            rule = rule.substr(pos + 1, rule.length);

          }



          switch (rule) {

            case 'required':

              if (i.val() === '') {

                ferror = ierror = true;

              }

              break;



            case 'minlen':

              if (i.val().length < parseInt(exp)) {

                ferror = ierror = true;

              }

              break;



            case 'email':

              if (!emailExp.test(i.val())) {

                ferror = ierror = true;

              }

              break;



            case 'checked':

              if (! i.is(':checked')) {

                ferror = ierror = true;

              }

              break;



            case 'regexp':

              exp = new RegExp(exp);

              if (!exp.test(i.val())) {

                ferror = ierror = true;

              }

              break;

          }

          i.next('.validation').html((ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');

        }

      });

      f.children('textarea').each(function() { // run all inputs



        var i = $(this); // current input

        var rule = i.attr('data-rule');



        if (rule !== undefined) {

          var ierror = false; // error flag for current input

          var pos = rule.indexOf(':', 0);

          if (pos >= 0) {

            var exp = rule.substr(pos + 1, rule.length);

            rule = rule.substr(0, pos);

          } else {

            rule = rule.substr(pos + 1, rule.length);

          }



          switch (rule) {

            case 'required':

              if (i.val() === '') {

                ferror = ierror = true;

              }

              break;



            case 'minlen':

              if (i.val().length < parseInt(exp)) {

                ferror = ierror = true;

              }

              break;

          }

          i.next('.validation').html((ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');

        }

      });

      if (ferror) return false;

      else var str = $(this).serialize();

      $.ajax({

        type: "POST",

        url: "<?=site_url('main/save')?>",

        data: str,

        dataType: "JSON",

        success: function(data) {

          //alert(msg);

          if (data.status == 'info') {

            $("#sendmessage").addClass("show");

            $("#errormessage").removeClass("show");

            $('.contactForm').find("input, textarea").val("");

            $("#sendmessage").html(data.msg);

          } else if(data.status == 'error') {

            $("#sendmessage").removeClass("show");

            $("#errormessage").addClass("show");

            $('#errormessage').html(data.msg);

          }

        }

      });

      return false;

    });



  });



</script>