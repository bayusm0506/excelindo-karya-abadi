<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="<?=site_url()?>">Home</a> <i class="icon-angle-right"></i></li>
            <li class="active">Event</li>
          </ul>
          <h2>Event</h2>
        </div>
      </div>
    </div>
  </div>
</section>
<style type="text/css">
  .media > .pull-left {
      margin-right: 29px;
  }
  .media .dateEl {
      display: inline-block;
      text-align: center;
      background: #f9f9f9;
      padding: 18px 0 25px 0;
      color: #173d51;
      font-size: 16px;
      font-weight: 700;
      width: 113px;
      text-transform: uppercase;
  }
  .media .dateEl em {
      display: block;
      color: #19a1dd;
      font-size: 42px;
      line-height: 1;
      margin-bottom: 5px;
      font-style: normal;
  }
  .media .media-heading a {
      color: #022235;
      font-size: 21px;
      text-transform: uppercase;
  }
  .media .longDate, .media .timeEl {
      display: inline-block;
      font-size: 14px;
      line-height: 14px;
      font-family: 'PT Sans Narrow', sans-serif;
      font-weight: 700;
      color: #636465;
      text-transform: uppercase;
      min-height: 16px;
  }
</style>
<section id="content">
  <div class="container">
    <div class="row">
      <?php
        $cekEvent = $event->num_rows();
        if ($cekEvent > 0) {
          $_array = $event->result_array();
          foreach ($_array as $p) {
            $expl_date = explode(" ", tgl_indo($p['tgl_event']));
            $keterangan =(strip_tags($p['keterangan'])); 
            $isi = substr($keterangan,0,150); 
            $isi = substr($keterangan,0,strrpos($isi," ")); 
            if (trim($isi)==''){
              $isi_event = 'Maaf, Tidak Ada ditemukan Informasi Dalam bentuk Teks';
            }else{
              $isi_event = $isi.' ....';
            }
      ?>
        <div class="span6">
          <div class="media">
            <a class="pull-left" href="<?=base_url().'event/detail/'.$p['jdl_seo']?>"><span class="dateEl"><em><?=$expl_date[0]?></em><?=$expl_date[1]?></span></a>
            <div class="media-body">
              <h4 class="media-heading">
                <a href="<?=base_url().'event/detail/'.$p['jdl_seo']?>"><?=$p['judul']?></a>
              </h4>
              <div class="meta-data">
                <span class="longDate"><?=tgl_indo($p['tgl_event'])?></span> <span class="timeEl"> <?=$p['jam_range']?></span>
              </div>
              <?=$isi_event?>
            </div><!-- / media-body -->
          </div><!-- / media -->
        </div>
      <?php 
          }
        } 
      ?>
    </div>
    <div id="pagination">
      <?php echo $this->pagination->create_links(); ?>
    </div><br />
    <?php
      $cekChoose = getChoose()->num_rows();
      if ($cekChoose > 0) {
    ?>
    <div class="row">
      <div class="span12 aligncenter">
        <?php if(get_cookie('lang_is') === 'en'){ ?>
          <h3 class="title">WHY CHOOSE <strong>US</strong></h3>
        <?php }else{ ?>
          <h3 class="title">Mengapa Memilih <strong>Kami</strong></h3>
        <?php } ?>
      </div>
    </div>
    <div class="row">
      <?php
        $array_choose = getChoose()->result_array();
        foreach ($array_choose as $p) {
          //$explodeJudul = explode(" ", $p['judul']);
      ?>
      <div class="span4">
        <div class="service-box aligncenter flyLeft">
          <div class="icon">
            <i class="<?=$p['icon']?>"></i>
          </div>
          <h5><span class="colored"> <?=$p['judul']?></span></h5>
          <p>
            <?=$p['keterangan']?>
          </p>

        </div>
      </div>
      <?php } ?>
    </div>
    <?php
      }
    ?>
  </div>
</section>