<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="<?=site_url()?>">Home</a> <i class="icon-angle-right"></i></li>
            <li class="active">Facilities</li>
          </ul>
          <h2>Facilities</h2>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="content">
  <div class="container">
    <div class="row">
      <div class="span6">
        <div class="box flyIn">
          <div class="icon">
            <i class="ico icon-circled icon-bglight icon-dropbox icon-3x"></i>
          </div>
          <div class="text">
            <h4>Rich of <strong>Features</strong></h4>
            <p>
              Lorem ipsum dolor sit amet, has ei ipsum scaevola deseruisse am sea facilisis.
            </p>
            <a href="#">Learn More</a>
          </div>
        </div>
        <!-- Description -->
        <h4>Office</h4>
        <dl>
          <dt>Description lists</dt>
          <dd>A description list is perfect for defining terms.</dd>
          <dt>Euismod</dt>
          <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
          <dd>Donec id elit non mi porta gravida at eget metus.</dd>
          <dt>Malesuada porta</dt>
          <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
        </dl>
      </div>

      <!-- Horizontal Description -->
      <div class="span6">
        <div class="box flyLeft">
          <div class="icon">
            <i class="ico icon-circled icon-bgprimary icon-code icon-3x"></i>
          </div>
          <div class="text">
            <h4>Valid <strong>Code</strong></h4>
            <p>
              Lorem ipsum dolor sit amet, has ei ipsum scaevola deseruisse am sea facilisis.
            </p>
            <a href="#">Learn More</a>
          </div>
        </div>
        <h4>Workshop</h4>
        <dl class="dl-horizontal">
          <dt>Description lists</dt>
          <dd>A description list is perfect for defining terms.</dd>
          <dt>Euismod</dt>
          <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
          <dd>Donec id elit non mi porta gravida at eget metus.</dd>
          <dt>Malesuada porta</dt>
          <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
          <dt>Felis euismod semper eget lacinia</dt>
          <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
        </dl>
      </div>
    </div>
  </div>
</section>