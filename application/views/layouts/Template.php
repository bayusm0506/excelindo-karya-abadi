<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no'/>
  <meta name="robots" content="index, follow">
  <meta name="description" content="<?php echo $description; ?>">
  <meta name="keywords" content="<?php echo $keywords; ?>">
  <meta name="author" content="<?php echo $title; ?>">
  <meta http-equiv="imagetoolbar" content="no">
  <meta name="language" content="Indonesia">
  <meta name="revisit-after" content="7">
  <meta name="webcrawlers" content="all">
  <meta name="rating" content="general">
  <meta name="spiders" content="all">

  <link rel="shortcut icon" href="<?php echo base_url()?>asset/images/<?php echo $favicon; ?>" />
  <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="rss.xml" />

  <!-- css -->
  <link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=EB+Garamond" />
  <link href="<?php echo base_url(); ?>asset/frontend/css/bootstrap.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>asset/frontend/css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>asset/frontend/css/flexslider.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>asset/frontend/css/prettyPhoto.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>asset/frontend/css/camera.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>asset/frontend/css/jquery.bxslider.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>asset/frontend/css/style.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>asset/frontend/css/owl.carousel.css" rel="stylesheet" />

  <!-- Theme skin -->
  <link href="<?php echo base_url(); ?>asset/frontend/color/default.css" rel="stylesheet" />

  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/message.css">
  
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>asset/frontend/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>asset/frontend/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>asset/frontend/ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>asset/frontend/ico/apple-touch-icon-57-precomposed.png" />

  <script src="<?php echo base_url(); ?>asset/frontend/js/jquery.js"></script>
  <script src="<?php echo base_url(); ?>asset/admin/dist/js/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/jquery-ui.min.css">
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url(); ?>asset/frontend/contactform/contactform.js"></script>
  
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/image-gallery/css/blueimp-gallery.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/image-gallery/css/blueimp-gallery-indicator.css">
  <script src="<?php echo base_url(); ?>asset/admin/dist/js/ajax.js"></script>
  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>asset/image-gallery/css/demo/demo.css"> -->
  <!-- =======================================================
    Theme Name: Eterna
    Theme URL: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
  <?php echo $this->recaptcha->getScriptTag(); ?>
  
  
  
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c78956d3341d22d9ce6b69a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->




</head>

<body>
  <div class="messsage-container" style="display:none">
    <div class="loading-temp loading-show"></div>
    <div class="center-loading"><a href="#" style="font-size:24px" class="btn btn-default btn-sm">Proses <i class="icon-spinner" style="font-size:24px"></i></a></div>
  </div>
  <div class="message-container" style="display:none">
    <div class="center-message">
     <div class="alert alert-danger" id="container-m" role="alert"> <button id="remove-message" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 id="t-message"></h4> <p id="m-message"></p> </div> 
    </div>
  </div>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2&appId=1835884626522886&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <div id="wrapper">
    <header>
      <div class="top">
        <div class="container">
          <div class="row">
            <div class="span6">
              <?php
                $identity = $this->MenuUtama_model->identitas()->row_array();
              ?>
              <p class="topcontact"><i class="icon-phone"></i> Telp : <a href="#"><?=$identity['no_telp']?></a> | <i class="icon-mobile-phone"></i> Whatsapp : <a href="https://api.whatsapp.com/send?phone=<?=$identity['whatsapp']?>&amp;text=Halo <?=substr($identity['nama_website'],0,25)?>, saya tertarik menggunakan jasa Anda. Bisa bantu saya?">+<?=$identity['whatsapp']?></a></p>
            </div>
            <div class="span6">
              <?php if(get_cookie('lang_is') === 'en'){ ?>

              <?php }else{ ?>

              <?php } ?>
              <!-- <div class="pull-right" style="color: #fff;">
              |

                <a href="<?php echo site_url('lang_setter/set_to/indonesia');?>"> <img src="<?=base_url()?>asset/images/id.svg" style="width:40px; height: 20px;"></a> - 
                <a href="<?php echo site_url('lang_setter/set_to/english');?>"> <img src="<?=base_url()?>asset/images/gb.svg" style="width:40px; height: 20px;"></a>
              </div> -->
              <ul class="social-network">
                <li><a href="<?=$identity['facebook']?>" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-white" target="_blank"></i></a></li>
                <li><a href="<?=$identity['twitter']?>" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-white" target="_blank"></i></a></li>
                <li><a href="<?=$identity['youtube']?>" data-placement="bottom" title="Youtube"><i class="icon-youtube icon-white" target="_blank"></i></a></li>
              </ul>

            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row nomargin">
          <div class="span4">
            <div class="logo">
              <?php
                $logo = $this->LogoWebsite_model->logo()->result_array();
                foreach ($logo as $p) {
                  echo "<a href='".base_url()."'><img src='".base_url()."asset/logo/$p[logo]' alt='Logo PT. Abbas Bumi Perkasa'></a>";
                }
              ?>
            </div>
          </div>
          <div class="span8">
            <div class="navbar navbar-static-top">
              <div class="navigation">
                <nav>
                  <ul class="nav topnav">
                    <li>
                      <?php if(get_cookie('lang_is') === 'en'){ ?>
                        <a href="<?php echo base_url(); ?>"><i class="icon-home"></i> HOME</i></a>
                      <?php }else{ ?>
                        <a href="<?php echo base_url(); ?>"><i class="icon-home"></i> BERANDA</i></a>
                      <?php } ?>
                    </li>
                    <?php
                      // $menuatas = $this->MenuWebsite_model->getByMenuUtama();
                      // foreach ($menuatas->result_array() as $row){
                      //     $dropdown = $this->MenuWebsite_model->getDropdownMenu($row['id_menu'])->num_rows();
                      //     if ($dropdown == 0){
                      //       echo "<li><a href='".site_url()."$row[link]'>$row[nama_menu]</a></li>";
                      //     }else{
                      //       echo "<li class='dropdown'>
                      //             <a href='".site_url()."$row[link]'>$row[nama_menu] <i class='icon-angle-down'></i></a>
                      //             <ul class='dropdown-menu'>";
                      //               $dropmenu = $this->model_menu->dropdown_menu($row['id_menu']);
                      //               foreach ($dropmenu->result_array() as $row){
                      //                   echo "<li><a href='".site_url()."$row[link]'>$row[nama_menu]</a></li>";
                      //               }
                      //             echo "</ul>
                      //           </li>";
                      //     }
                      // }
                    ?>
                    <?php
                      $menuatas = $this->MenuWebsite_model->getByMenuUtama();
                      foreach ($menuatas->result_array() as $row){
                          $dropdown = $this->MenuWebsite_model->getDropdownMenu($row['id_menu'])->num_rows();
                          if ($dropdown == 0){
                            echo "<li><a href='".site_url()."$row[link]'>$row[nama_menu]</a></li>";
                          }else{
                            echo "<li class='dropdown'>
                                  <a href='".site_url()."$row[link]'>$row[nama_menu] <i class='icon-angle-down'></i></a>
                                  <ul class='dropdown-menu'>";
                                    $dropmenu = $this->model_menu->dropdown_menu($row['id_menu']);
                                    foreach ($dropmenu->result_array() as $row){
                                        $cek_menu = $this->model_menu->dropdown_menu($row['id_menu'])->num_rows();
                                        if ($cek_menu == 0) {
                                          echo "<li><a href='".site_url()."$row[link]'>$row[nama_menu]</a></li>";
                                        }else{
                                          echo "<li class='dropdown'>
                                              <a href='".site_url()."$row[link]'>$row[nama_menu] <i class='icon-angle-right'></i></a>
                                              <ul class='dropdown-menu sub-menu-level1'>";
                                              $submenu = $this->model_menu->dropdown_menu($row['id_menu']);
                                              foreach ($submenu->result_array() as $p) {
                                                echo "<li><a href='".site_url()."$p[link]'>$p[nama_menu]</a></li>";
                                              }
                                              echo "</ul></li>";
                                        }
                                    }
                                  echo "</ul>
                                </li>";
                          }
                      }
                    ?>
                  </ul>
                </nav>
              </div>
              <!-- end navigation -->
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- end header -->
    <?php 
      echo $contents; 
      $this->Dashboard_model->visitor();
      $clients = $this->Clients_model->clients()->num_rows();
      if($clients > 0){
    ?>
    <section id="clients">
      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="clients-slider owl-carousel">
              <?php 
                $produk = $this->ProductGallery_model->getProductSlide()->result_array();
                foreach ($produk as $key) {
              ?>
              <a href="<?=base_url()?>product/detail/<?=$key['id_pg']?>/<?=$key['seo']?>" title="<?=$key['jdl_pg']?>">
                <img src="<?php echo base_url(); ?>asset/produk_gallery/<?=$key['gbr_pg']?>" alt="<?=$key['jdl_pg']?>">
              </a>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php } ?>
    <footer>
      <div class="container">
        <div class="row">

          <div class="span4">
            <div class="widget">
              <?php
                $logo = $this->LogoWebsite_model->logo()->result_array();
                foreach ($logo as $p) {
                  echo "<a href='".base_url()."'><img src='".base_url()."asset/logo/$p[logo]' alt='Logo PT. Abbas Bumi Perkasa'></a>";
                }
              ?>
              
            </address>
              <br />
              <br />
              <?=$identity['keterangan']?>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              
              <?php if(get_cookie('lang_is') === 'en'){ ?>
                <h5 class="widgetheading">Link Useful</h5>
              <?php }else{ ?>
                <h5 class="widgetheading">Tautan Berguna</h5>
              <?php } ?>
              <ul class="link-list">
                <?php
                  $LinkUsefull = $this->MenuHalamanBaru_model->usefull()->result_array();
                  foreach($LinkUsefull as $p){
                ?>
                  <li><a href="<?=base_url()?>page/detail/<?=$p['judul_seo']?>"><?=$p['judul']?></a></li>
                <?php } ?>
		<li><a href="<?=base_url()?>product">Produk</a></li>
                <li><a href="<?=base_url()?>servis">Layanan</a></li>
                <li><a href="<?=base_url()?>video">Video</a></li>
                <li><a href="<?=base_url()?>contact">Hubungi Kami</a></li>
              </ul>

            </div>
          </div>
          
          <div class="span4">
            
            <?php if(get_cookie('lang_is') === 'en'){ ?>
              <h5 class="widgetheading">Contact Us</h5>
            <?php }else{ ?>
              <h5 class="widgetheading">Kontak Kami</h5>
            <?php } ?>
            <address>
              <!-- <strong>Twitter, Inc.</strong><br> -->
              <?=$identity['address']?>
            </address>
            <address>
              <strong>Phone</strong><br>
              <a href="#"><?=$identity['no_telp']?></a> - 
              <a href="https://api.whatsapp.com/send?phone=<?=$identity['whatsapp']?>&amp;text=Halo <?=substr($identity['nama_website'],0,25)?>, saya tertarik menggunakan jasa Anda. Bisa bantu saya?">+<?=$identity['whatsapp']?></a>
            </address>
            <address>
              <strong>Email</strong><br>
              <a href="mailto:#"><?=$identity['email']?></a>
            </address>
          </div>
        </div>
      </div>
      <div id="sub-footer">
        <div class="container">
          <div class="row">
            <div class="span6">
              <div class="copyright">
                <p><span>&copy; <?=date('Y')?> <?=$identity['nama_website']?>. All right reserved</span></p>
              </div>

            </div>

            <div class="span6">
              <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Eterna
                -->
                Designed by <a href="#">Excelindo Design</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bglight icon-2x active"></i></a>

  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="<?php echo base_url(); ?>asset/frontend/js/jquery.easing.1.3.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/bootstrap.js"></script>

  <script src="<?php echo base_url(); ?>asset/frontend/js/modernizr.custom.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/toucheffects.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/google-code-prettify/prettify.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/jquery.bxslider.min.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/camera/camera.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/camera/setting.js"></script>

  <script src="<?php echo base_url(); ?>asset/frontend/js/jquery.prettyPhoto.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/portfolio/jquery.quicksand.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/portfolio/setting.js"></script>

  <script src="<?php echo base_url(); ?>asset/frontend/js/jquery.flexslider.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/animate.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/inview.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="<?php echo base_url(); ?>asset/frontend/js/custom.js"></script>
  <script src="<?php echo base_url(); ?>asset/frontend/js/owl.carousel.min.js"></script>

  <!-- Images Gallery -->
  <script src="<?php echo base_url(); ?>asset/image-gallery/js/blueimp-helper.js"></script>
  <script src="<?php echo base_url(); ?>asset/image-gallery/js/blueimp-gallery.js"></script>
  <script src="<?php echo base_url(); ?>asset/image-gallery/js/blueimp-gallery-fullscreen.js"></script>
  <script src="<?php echo base_url(); ?>asset/image-gallery/js/blueimp-gallery-indicator.js"></script>
  <script src="<?php echo base_url(); ?>asset/image-gallery/js/jquery.blueimp-gallery.js"></script>
  <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCULbitqHzHI6IZ-ASgOwdm6RABgtpkBsU&callback=initMap"></script> -->

  <script type="text/javascript">
    $("button#remove-message").click(function(){
      $("div.message-container").hide();
    })
    // Partners slider
    $('.clients-slider').owlCarousel({
      loop:true,
      margin:15,
      dots : false,
      nav: true,
      navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
      autoplay : true,
      responsive:{
        0:{
          items:2
        },
        480:{
          items:4
        },
        992:{
          items:6
        }
      }
    });

    // var url = window.location;
    // // for sidebar menu entirely but not cover treeview
    // $('ul.topnav a').filter(function() {
    //   return this.href == url;
    // }).parent().addClass('active');

    // // for treeview
    // $('ul.dropdown-menu a').filter(function() {
    //   return this.href == url;
    // }).closest('.dropdown').addClass('active');

    

    var url = window.location;

    // for sidebar menu entirely but not cover treeview
    $('ul.topnav a').filter(function() {
       return this.href == url;
    }).parent().addClass('active');

    // for treeview
    $('ul.dropdown-menu a').filter(function() {
       return this.href == url;
    }).parentsUntil(".topnav > .dropdown-menu").addClass('active');

  </script>
</body>
</html>
