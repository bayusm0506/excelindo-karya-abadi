<div class="span4">
  <aside class="left-sidebar">

    <div class="widget">
      <form method="get" action="<?=base_url()?>product">
        <div class="input-append">
          <input name="nama_produk" class="span2" id="appendedInputButton" type="text" placeholder="Cari Produk">
          <button name="frame" class="btn btn-theme" type="submit">Cari</button>
        </div>
      </form>
    </div>
    <div class="widget">
      <h5 class="widgetheading">Kategori Produk</h5>
      <ul class="cat">
         <?php
          $mg_array = $this->MenuGroup_model->getMenu()->result_array();
          foreach($mg_array as $data){
        ?>
          <li><i class="icon-angle-right"></i> <a href="<?=base_url()?>product/category/<?=$data['seo']?>" title="<?=$data['nama_group_menu']?>"><?=$data['nama_group_menu']?><span> (<?php $t_gal = $this->ProductGallery_model->getProductCount($data['id_group_menu'])->num_rows(); if($t_gal > 0){echo $t_gal;}else{echo "0";}?>)</span></a></li>
        <?php
          }
        ?>
      </ul>
    </div>
    <div class="widget">
      <?php if(get_cookie('lang_is') === 'en'){ ?>
        <h5 class="widgetheading">Contact information<span></span></h5>
      <?php }else{ ?>
        <h5 class="widgetheading">Informasi Kontak<span></span></h5>
      <?php } ?>
      
      <?php
        $identity = $this->MenuUtama_model->identitas()->row_array();
      ?>
      <ul class="contact-info">
        <li>
          <?php if(get_cookie('lang_is') === 'en'){ ?>
            <label>Address :</label> 
          <?php }else{ ?>
            <label>Alamat :</label> 
          <?php } ?>                
          <?=$identity['address']?>
        </li>
        <li>
          <?php if(get_cookie('lang_is') === 'en'){ ?>
            <label>Phone :</label> 
          <?php }else{ ?>
            <label>No. HP :</label> 
          <?php } ?>
          <?=$identity['no_telp']?> / <a href="https://api.whatsapp.com/send?phone=<?=$identity['whatsapp']?>&amp;text=Halo <?=substr($identity['nama_website'],0,25)?>, saya tertarik menggunakan jasa Anda. Bisa bantu saya?">+<?=$identity['whatsapp']?></a>
        </li>
        <li>
          <label>Email : </label> 
          <?=$identity['email']?>
        </li>
      </ul>

    </div>
    <div class="widgeti">
      <div class="box flyIn animated fadeInUp">
        <div class="icon">
          <i class="ico icon-circled icon-bgdark icon-time active icon-3x"></i>
        </div>
        <div class="text">
                            <h4 class="">Jam <strong>Buka</strong></h4>
                          <p>
            Senin - Sabtu: 08:00 - 16:00                </p>
        </div>
      </div>
    </div>
  </aside>
</div>

<script type="text/javascript">
  $( "#appendedInputButton" ).autocomplete({
    source: "<?php echo site_url('product/get_autocomplete/?');?>"
  });
</script>