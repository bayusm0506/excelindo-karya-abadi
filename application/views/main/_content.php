<link rel="stylesheet" href="<?=base_url()?>asset/frontend/image-gallery/css/blueimp-gallery.css">
<link rel="stylesheet" href="<?=base_url()?>asset/frontend/image-gallery/css/blueimp-gallery-indicator.css">
<section id="content">
  <div class="container">
    <?php
      $identity = $this->MenuUtama_model->identitas()->row_array();
      $welcome = $this->Welcome_model->welcome()->row_array();
    ?>
    <div class="row">
      <div class="span12">
        <div class="row">
          <div class="span4">
            <div class="box flyLeft">
              <div class="icon">
                <i class="ico icon-circled icon-bgdark icon-mobile-phone active icon-3x"></i>
              </div>
              <div class="text">
                <?php if(get_cookie('lang_is') === 'en'){ ?>
                  <h4>Phone <strong>Number</strong></h4>
                <?php }else{ ?>
                  <h4>Nomor <strong>HP</strong></h4>
                <?php } ?>
                <p>
                  +<?=$identity['whatsapp']?>
                </p>
              </div>
            </div>
          </div>
          <div class="span4">
            <div class="box flyIn">
              <div class="icon">
                <i class="ico icon-circled icon-bgdark icon-time active icon-3x"></i>
              </div>
              <div class="text">
                <?php if(get_cookie('lang_is') === 'en'){ ?>
                  <h4>Opening <strong>Times</strong></h4>
                <?php }else{ ?>
                  <h4>Jam <strong>Buka</strong></h4>
                <?php } ?>
                <p>
                  <?=$identity['open']?>
                </p>
              </div>
            </div>
          </div>
          <div class="span4">
            <div class="box flyRight">
              <div class="icon">
                <i class="ico icon-circled icon-bgdark icon-envelope-alt active icon-3x"></i>
              </div>
              <div class="text">
                <?php if(get_cookie('lang_is') === 'en'){ ?>
                  <h4>Email <strong>Address</strong></h4>
                <?php }else{ ?>
                  <h4>Alamat <strong>Email</strong></h4>
                <?php } ?>
                <p>
                  <?=$identity['email']?>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="span12">
        <div class="solidline"></div>
      </div>
    </div>
    <div class="row">
      <div class="span12">
        <h4><strong>Selamat Datang!</strong></h4>
        <?=$welcome['welcome']?>
        <?php
          $mg = $this->MenuGroup_model->getMenu()->num_rows();
          if ($mg > 0) {
        ?>
        <div class="row">
            <section id="team">
              <ul id="thumbs" class="team">
                <?php
                  $mg_array = $this->MenuGroup_model->getMenu()->result_array();
                  foreach($mg_array as $data){
                ?>
                <!-- Item Project and Filter Name -->
                <li class="item-thumbs span2 design" data-id="id-0" data-type="design">
                  <div class="team-box thumbnail" style="padding:0px;">
                    <a href="<?=base_url()?>product/category/<?=$data['seo']?>" title="<?=$data['nama_group_menu']?>"><img src="<?=base_url()?>asset/img_mg/<?=$data['gbr_mg']?>" alt="<?=$data['nama_group_menu']?>" style="height: 200px;"/></a>
                  </div>
                </li>
                <!-- End Item Project -->
                <?php
                  }
                ?>

              </ul>
            </section>
        </div>
        <?php
          }
        ?>
      </div>
      <div class="span12">
        <h4><strong>Produk</strong></h4>
        <div class="row">
            <section id="team">
              <ul id="thumbs" class="team">
                <!-- Item Project and Filter Name -->
                <?php
                  $pg_array = $this->ProductGallery_model->getProduct()->result_array();
                  foreach ($pg_array as $key) {
                ?>
                  <li class="item-thumbs span2 design" data-id="id-0" data-type="design">
                    <div class="team-box thumbnail" style="padding:5px 5px 10px;">
                      <a  href="<?=base_url()?>asset/produk_gallery/<?=$key['gbr_pg']?>" title="<?=$key['jdl_pg'] ?>" data-gallery >
                        <img src="<?=base_url()?>asset/produk_gallery/<?=$key['gbr_pg']?>" style="max-width:100%;">
                      </a>
                    </div>
                  </li>
                <?php
                  }
                ?>
                <!-- End Item Project -->
              </ul>
            </section>

          </div>
      </div>
      <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev" style="color:#fff">‹</a>
        <a class="next" style="color:#fff">›</a>
        <a class="close" style="color:#fff">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
      </div>
    </div>

    <?php
      $testimoni = $this->Testimoni_model->getTesti()->num_rows();
      if ($testimoni > 0) {
    ?>
    <div class="row">
      <div class="span12 aligncenter">
        <h3 class="title">Testimoni <strong></strong></h3>
        <div class="blankline30"></div>

        <ul class="bxslider">
          <?php
            $testi = $this->Testimoni_model->getTesti()->result_array();
            foreach($testi as $testimon){
          ?>
          <li>
            <blockquote>
              <?=$testimon['keterangan']?>
            </blockquote>
            <div class="testimonial-autor">
              <img src="<?php echo base_url(); ?>asset/img_testi/<?=$testimon['gambar']?>" alt="<?=$testimon['nama']?>" width="40" height="40" class="img-circle"/>
              <h4>Hillary Doe</h4>
              <a href="<?=$testimon['website']?>" target="_blank"><?=$testimon['website']?></a>
            </div>
          </li>
          <?php } ?>
        </ul>

      </div>
    </div>
    <?php } ?>
  </div>
</section>
<script src="<?=base_url()?>asset/frontend/asset/image-gallery/js/blueimp-helper.js"></script>
<script src="<?=base_url()?>asset/frontend/asset/image-gallery/js/blueimp-gallery.js"></script>
<script src="<?=base_url()?>asset/frontend/asset/image-gallery/js/blueimp-gallery-fullscreen.js"></script>
<script src="<?=base_url()?>asset/frontend/asset/image-gallery/js/blueimp-gallery-indicator.js"></script>
<script src="<?=base_url()?>asset/frontend/image-gallery/js/jquery.blueimp-gallery.js"></script>