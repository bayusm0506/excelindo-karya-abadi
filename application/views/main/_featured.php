<?php

  $slides = $this->Slider_model->slider()->num_rows();

  $identity = $this->MenuUtama_model->identitas()->row_array();

  // print_r($slides);

  // exit;

  if ($slides > 0) {

?>

<!-- section featured -->

<section id="featured">



  <!-- slideshow start here -->



  <div class="camera_wrap" id="camera-slide">



    <!-- slide 1 here -->

    <?php

      $slide = $this->Slider_model->slider()->result_array();

      foreach ($slide as $p) {

    ?>

    <div data-src="<?php echo base_url(); ?>asset/img_slider/<?=$p['bg_slide']?>">

      <div class="camera_caption fadeFromLeft">

        <div class="container">

          <div class="row">

            <div class="span6">

              <h2 class="animated fadeInDown"><strong><?=$p['jdl_slide']?> <span class="colored"><?=$p['jdl_color']?></span></strong></h2>

              <p class="animated fadeInUp"> <?=$p['keterangan']?></p>

              <?php if(get_cookie('lang_is') === 'en'){ ?>

                <a href="https://api.whatsapp.com/send?phone=<?=$identity['whatsapp']?>&amp;text=Hello Abbas Bumi Perkasa, I am interested in using your service. Could you please help me?" class="btn btn-theme btn-large animated fadeInUp">

                  <i class="icon-phone"></i> Contact Us

                </a>

              <?php }else{ ?>

                <a href="https://api.whatsapp.com/send?phone=<?=$identity['whatsapp']?>&amp;text=Halo <?=substr($identity['nama_website'],0,25)?>, saya tertarik menggunakan jasa Anda. Bisa bantu saya?" class="btn btn-theme btn-large animated fadeInUp">

                  <i class="icon-phone"></i> Kontak Kami

                </a>

              <?php } ?>

              

              <?php if(get_cookie('lang_is') === 'en'){ ?>

                <a href="<?php echo site_url('services'); ?>" class="btn btn-default btn-large animated fadeInUp">

                  <i class="icon-services"></i> Our Services

                </a>

              <?php }else{ ?>

                <a href="<?php echo site_url('services'); ?>" class="btn btn-default btn-large animated fadeInUp">

                  <i class="icon-services"></i> Pelayanan Kami

                </a>

              <?php } ?>

            </div>

            <div class="span6">

              <img src="<?php echo base_url(); ?>asset/img_slider/<?=$p['gbr_slide']?>" alt="<?=$p['jdl_slide']?>" class="animated bounceInDown delay1"/>

            </div>

          </div>

        </div>

      </div>

    </div>

    <?php

      }

    ?>

  </div>



  <!-- slideshow end here -->



</section>

<!-- /section featured -->

<?php

  }

?>