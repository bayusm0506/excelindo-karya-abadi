<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="index.html">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="#">Pages</a> <i class="icon-angle-right"></i></li>
            <li class="active"><?=ucwords($record['judul'])?></li>
          </ul>
          <h2><?=ucwords($record['judul'])?></h2>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="content" style="background-image: url(<?=base_url()?>asset/foto_statis/<?=$record['gambar']?>); background-repeat: no-repeat; background-size: cover;">
  <div class="container">
    <div class="row">
      <?php echo $this->load->view("main/_category"); ?>
      <div class="span8">
        <article>
          <div class="row">

            <div class="span8">
              <div class="post-entry">
                <?=$record['isi_halaman']?>
              </div>
            </div>
          </div>
        </article>
      </div>

    </div>

    <!-- divider -->
    <div class="row">
      <div class="span12">
        <div class="solidline"></div>
      </div>
    </div>
    <!-- end divider -->

    <?php
      $cekChoose = getChoose()->num_rows();
      if ($cekChoose > 0) {
    ?>
    <div class="row">
      <div class="span12 aligncenter">
        <h3 class="title">WHY CHOOSE <strong>US</strong></h3>
      </div>
    </div>
    <div class="row">
      <?php
        $array_choose = getChoose()->result_array();
        foreach ($array_choose as $p) {
          //$explodeJudul = explode(" ", $p['judul']);
      ?>
      <div class="span4">
        <div class="service-box aligncenter flyLeft">
          <div class="icon">
            <i class="<?=$p['icon']?>"></i>
          </div>
          <h5><span class="colored"> <?=$p['judul']?></span></h5>
          <p>
            <?=$p['keterangan']?>
          </p>

        </div>
      </div>
      <?php } ?>
    </div>
    <?php
      }
    ?>

  </div>
</section>