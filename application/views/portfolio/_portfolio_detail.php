<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="index.html">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="<?=site_url('portfolio')?>">Portfolio</a> <i class="icon-angle-right"></i></li>
            <li class="active"><?=$record['jdl_gallery']?></li>
          </ul>
          <h2><?=$record['jdl_gallery']?></h2>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="content">
  <div class="container">
  	<div class="row">
      <div class="span6">
        <!-- Description -->
    	 <article class="noborder">
          <div class="top-wrapper">
            <!-- start flexslider -->
            <div class="portfolio-detail">
              <img src="<?=base_url()?>asset/img_galeri/<?=$record['gbr_gallery']?>" alt="<?=$record['jdl_gallery']?>" />
            </div>
            <!-- end flexslider -->
          </div>
        </article>
        <?php if($record['gbr_gallery2'] != ''){ ?>
        <article class="noborder">
          <div class="top-wrapper">
            <!-- start flexslider -->
            <div class="portfolio-detail">
              <img src="<?=base_url()?>asset/img_galeri/<?=$record['gbr_gallery2']?>" alt="<?=$record['jdl_gallery']?>" />
            </div>
            <!-- end flexslider -->
          </div>
        </article>
        <?php } ?>

        <?php if($record['gbr_gallery3'] != ''){ ?>
        <article class="noborder">
          <div class="top-wrapper">
            <!-- start flexslider -->
            <div class="portfolio-detail">
              <img src="<?=base_url()?>asset/img_galeri/<?=$record['gbr_gallery3']?>" alt="<?=$record['jdl_gallery']?>" />
            </div>
            <!-- end flexslider -->
          </div>
        </article>
        <?php } ?>
      </div>

      <!-- Horizontal Description -->
      <div class="span6">
        <h4>Project Info</h4>
        <?=$record['keterangan']?>
      </div>
    </div>
    <div class="row">
      <div class="span12">
        <div class="row">
          <div class="span12">
            <h4>Related works</h4>
          </div>
          <div class="grid related-works cs-style-4">
          	<?php
          		foreach($related as $p){
          	?>
            <div class="span2">
              <div class="item">
                <figure>
                  <div><img src="<?=base_url()?>asset/img_galeri/<?=$p['gbr_gallery']?>" alt="<?=$p['jdl_gallery']?>" /></div>
                  <figcaption>
                    <div>
                      <span>
										<a href="<?=base_url()?>asset/img_galeri/<?=$p['gbr_gallery']?>" alt="<?=$p['jdl_gallery']?>" data-pretty="prettyPhoto[gallery1]" title="<?=$p['jdl_gallery']?>"><i class="icon-plus icon-circled icon-bglight"></i></a>
										</span>
                      <span>
										<a href="<?=base_url()?>portfolio/detail/<?=$p['gallery_seo']?>"><i class="icon-file icon-circled icon-bglight"></i></a>
										</span>
                    </div>
                  </figcaption>
                </figure>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>

      </div>

    </div>

  </div>
</section>