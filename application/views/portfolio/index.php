<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="index.html">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="<?=site_url('portfolio')?>">Portfolio</a> <i class="icon-angle-right"></i></li>
            <li class="active"><?=getTitle()?></li>
          </ul>
          <h2>Portfolio</h2>
          <h5>Our Buildings and Projects</h5>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="content">
  <div class="container">
    <div class="row">
      <div class="span12">
        <ul class="portfolio-categ filter">
        	<li class="all active"><a href="#">All</a></li>
	        <?php
	          	foreach($kategori as $p){
	          		echo "<li class='$p[album_seo]$p[id_album]'><a href='#'' title='$p[jdl_album]'>$p[jdl_album]</a></li>";
	          	}
	        ?>
        </ul>

        <div class="clearfix"></div>
        <div class="row">
          <section id="projects">
            <ul id="thumbs" class="grid cs-style-4 portfolio">
              <?php
              	$no = 0;
              	foreach($project as $pro){
              ?>
              <!-- Item Project and Filter Name -->
              <li class="item-thumbs span3 design" data-id="id-<?=$no++;?>" data-type="<?=$pro['album_seo'].$pro['id_album']?>">
                <div class="item">
                  <figure>
                    <div><img src="<?=base_url()?>asset/img_galeri/<?=$pro['gbr_gallery']?>" alt="" /></div>
                    <figcaption>
                      <div>
                        <span>
										<a href="<?=base_url()?>asset/img_galeri/<?=$pro['gbr_gallery']?>" data-pretty="prettyPhoto[gallery1]" title="<?=$pro['jdl_gallery']?>"><i class="icon-plus icon-circled icon-bglight icon-2x"></i></a>
										</span>
                        <span>
										<a href="<?=base_url()?>portfolio/detail/<?=$pro['gallery_seo']?>"><i class="icon-file icon-circled icon-bglight icon-2x"></i></a>
										</span>
                      </div>
                    </figcaption>
                  </figure>
                </div>
              </li>
              <!-- End Item Project -->
              <?php } ?>

            </ul>
          </section>

        </div>
      </div>
    </div>
  </div>
</section>