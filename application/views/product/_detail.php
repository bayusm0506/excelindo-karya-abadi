<section id="inner-headline">

  <div class="container">

    <div class="row">

      <div class="span12">

        <div class="inner-heading">

          <ul class="breadcrumb">

            <li><a href="<?=base_url()?>">Home</a> <i class="icon-angle-right"></i></li>

            <li><a href="#">Produk</a><i class="icon-angle-right"></i></li>

            <li class="active"><?=ucwords($title)?></li>

          </ul>

          <h2><?=$title?></h2>

        </div>

      </div>

    </div>

  </div>

</section>



<section id="content" style="background-image: url(); background-repeat: no-repeat; background-size: cover;">

  <div class="container">

    <div class="row">

      <?php echo $this->load->view("main/_category"); ?>

      <div class="span5">

        <article class="single">

            <div class="row">



              <div class="span5">

                <div class="post-image">

                  <div class="post-heading">

                    <h3><a href="#"><?=$record['jdl_pg']?></a></h3>

                  </div>

                  <img src="<?=base_url()?>asset/produk_gallery/<?=$record['gbr_pg']?>" alt="<?=$record['jdl_pg']?>" />

                </div>

                <?=$record['ket']?>

              </div>

            </div>

          </article>

      </div>

      <div class="span3">

          <h6>KIRIM PERMINTAAN HARGA</h6>

          <div id="sendmessage">Pesan Permintaan Anda telah terkirim, kami akan segera menghubungi Anda kembali</div>

          <div id="errormessage"></div>

            <form action="" method="post" role="form" class="contactForm">

            <div class="row">

              <div class="span3 form-group field">

                <input type="text" name="nama" id="name" placeholder="Nama" data-rule="minlen:4" data-msg="Tolong masukkan minimal 4 karakter" />

                <div class="validation"></div>

              </div>



              <div class="span3 form-group">

                <input type="email" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Tolong Masukkan Alamat Email yang valid" />

                <div class="validation"></div>

              </div>



              <div class="span3 form-group field">

                <input type="text" name="telephone" id="telephone" placeholder="Telepon (Optional)" data-rule="telephone" data-msg="Tolong Masukkan No. Telepon" />

                <div class="validation"></div>

              </div>



              <div class="span3 form-group">

                <input type="text" name="website" id="website" placeholder="Website (Optional)" data-rule="website" data-msg="Tolong Masukkan Website" />

                <div class="validation"></div>

              </div>

              <div class="span3 form-group">

                <input type="text" name="subjek" id="subject" placeholder="Subjek" data-rule="minlen:4" data-msg="Tolong Masukkan minimal 8 karakter di kolom" />

                <div class="validation"></div>

              </div>

              <div class="span3 form-group">

                <textarea name="isi_pesan" rows="5" data-rule="required" data-msg="Tolong Tulis Sesuatu buat kita" placeholder="Pesan"></textarea>

                <div class="g-recaptcha" data-sitekey="6Lfqk5AUAAAAAH6R51bU6x85N1vIzWYMcnZ_HQ7g" data-theme="light" data-type="image" ></div>              <div class="validation"></div>

                <div class="text-center">

                  <button class="btn btn-theme btn-medium margintop10" type="submit">Send a message</button>

                </div>

              </div>

            </div>

          </form>

      </div>

    </div>



    <!-- divider -->

    <div class="row">

      <div class="span12">

        <div class="solidline"></div>

      </div>

    </div>

    <!-- end divider -->

  </div>

</section>



<script type="text/javascript">

  jQuery(document).ready(function($) {

    "use strict";



    //Contact

    $('form.contactForm').submit(function() {

      var f = $(this).find('.form-group'),

        ferror = false,

        emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;



      f.children('input').each(function() { // run all inputs



        var i = $(this); // current input

        var rule = i.attr('data-rule');



        if (rule !== undefined) {

          var ierror = false; // error flag for current input

          var pos = rule.indexOf(':', 0);

          if (pos >= 0) {

            var exp = rule.substr(pos + 1, rule.length);

            rule = rule.substr(0, pos);

          } else {

            rule = rule.substr(pos + 1, rule.length);

          }



          switch (rule) {

            case 'required':

              if (i.val() === '') {

                ferror = ierror = true;

              }

              break;



            case 'minlen':

              if (i.val().length < parseInt(exp)) {

                ferror = ierror = true;

              }

              break;



            case 'email':

              if (!emailExp.test(i.val())) {

                ferror = ierror = true;

              }

              break;



            case 'checked':

              if (! i.is(':checked')) {

                ferror = ierror = true;

              }

              break;



            case 'regexp':

              exp = new RegExp(exp);

              if (!exp.test(i.val())) {

                ferror = ierror = true;

              }

              break;

          }

          i.next('.validation').html((ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');

        }

      });

      f.children('textarea').each(function() { // run all inputs



        var i = $(this); // current input

        var rule = i.attr('data-rule');



        if (rule !== undefined) {

          var ierror = false; // error flag for current input

          var pos = rule.indexOf(':', 0);

          if (pos >= 0) {

            var exp = rule.substr(pos + 1, rule.length);

            rule = rule.substr(0, pos);

          } else {

            rule = rule.substr(pos + 1, rule.length);

          }



          switch (rule) {

            case 'required':

              if (i.val() === '') {

                ferror = ierror = true;

              }

              break;



            case 'minlen':

              if (i.val().length < parseInt(exp)) {

                ferror = ierror = true;

              }

              break;

          }

          i.next('.validation').html((ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');

        }

      });

      if (ferror) return false;

      else var str = $(this).serialize();

      $.ajax({

        type: "POST",

        url: "<?=site_url('main/save')?>",

        data: str,

        dataType: "JSON",

        success: function(data) {

          //alert(msg);

          if (data.status == 'info') {

            $("#sendmessage").addClass("show");

            $("#errormessage").removeClass("show");

            $('.contactForm').find("input, textarea").val("");

            $("#sendmessage").html(data.msg);

          } else if(data.status == 'error') {

            $("#sendmessage").removeClass("show");

            $("#errormessage").addClass("show");

            $('#errormessage').html(data.msg);

          }

        }

      });

      return false;

    });



  });



</script>



