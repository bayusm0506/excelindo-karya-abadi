<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="<?=base_url()?>">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="#">Produk</a></li>
            <li class="active"></li>
          </ul>
          <h2><?=$title?></h2>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="content" style="background-image: url(); background-repeat: no-repeat; background-size: cover;">
  <div class="container">
    <div class="row">
      <?php echo $this->load->view("main/_category"); ?>
      <div class="span8">
        <?php
          foreach ($produk->result_array() as $pro) {
            $isi_produk =(strip_tags($pro['ket'])); 
            $isi = substr($isi_produk,0,155); 
            $isi = substr($isi_produk,0,strrpos($isi," ")); 
            if (trim($isi)==''){
              $isi_artikel = 'Maaf, Tidak Ada ditemukan Informasi Dalam bentuk Teks, Silahkan Melihat Detail Informasi..';
            }else{
              $isi_artikel = $isi.' .... <br />';
            }
        ?>
          <article>
            <div class="row">
              <div class="span2">
                <div class="post-image">
                  <img src="<?=base_url()?>asset/produk_gallery/<?=$pro['gbr_pg']?>" alt="<?=$pro['jdl_pg']?>" />
                </div>
              </div>
              <div class="span6">
                <div class="post-heading">
                  <h3><a href="<?=base_url()?>product/detail/<?=$pro['id_pg']?>/<?=$pro['seo']?>"><?=$pro['jdl_pg']?></a></h3>
                </div>
                <div class="post-entry">
                  <?=$isi_artikel?><br />
                  <a href="<?=base_url()?>product/detail/<?=$pro['id_pg']?>/<?=$pro['seo']?>" class="readmore">Selanjutnya <i class="icon-angle-right"></i></a>
                </div>
              </div>
            </div>
          </article>
        <?php
          }
        ?>
        <div id="pagination">
          <?php echo $this->pagination->create_links(); ?>
        </div>
      </div>

    </div>

    <!-- divider -->
    <div class="row">
      <div class="span12">
        <div class="solidline"></div>
      </div>
    </div>
    <!-- end divider -->

    <?php
      $cekChoose = getChoose()->num_rows();
      if ($cekChoose > 0) {
    ?>
    <div class="row">
      <div class="span12 aligncenter">
        <h3 class="title">WHY CHOOSE <strong>US</strong></h3>
      </div>
    </div>
    <div class="row">
      <?php
        $array_choose = getChoose()->result_array();
        foreach ($array_choose as $p) {
          //$explodeJudul = explode(" ", $p['judul']);
      ?>
      <div class="span4">
        <div class="service-box aligncenter flyLeft">
          <div class="icon">
            <i class="<?=$p['icon']?>"></i>
          </div>
          <h5><span class="colored"> <?=$p['judul']?></span></h5>
          <p>
            <?=$p['keterangan']?>
          </p>

        </div>
      </div>
      <?php } ?>
    </div>
    <?php
      }
    ?>

  </div>
</section>