<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="<?=base_url()?>">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="#">Layanan</a></li>
            <li class="active"></li>
          </ul>
          <h2><?=$title?></h2>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="content" style="background-image: url(); background-repeat: no-repeat; background-size: cover;">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="row">
            <section id="team">
              <div class="span3">
                <div class="service-box aligncenter flyIn">
                  <div class="icon">
                    <i class="icon-circled icon-bgsuccess icon-cloud icon-4x"></i>
                  </div>
                  <h5>Pelayanan <span class="colored">Kami</span></h5>
                </div>
              </div>
              <ul id="thumbs" class="team">
                <?php
                  $servis_array = $this->Servis_model->getServis()->result_array();
                  foreach ($servis_array as $key) {
                ?>
                <style type="text/css">
                  .image {
                    opacity: 1;
                    display: block;
                    width: 100%;
                    height: auto;
                    transition: .5s ease;
                    backface-visibility: hidden;
                  }

                  .middle {
                    transition: .5s ease;
                    opacity: 0;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                    -ms-transform: translate(-50%, -50%)
                  }

                  .containerx:hover .image {
                    opacity: 0.3;
                  }
                  
                  .containerx:hover .middle {
                    opacity: 1;
                  }

                  .text {
                    background-color: #4CAF50;
                    color: white;
                    font-size: 16px;
                    padding: 16px 32px;
                  }
                </style>
                  <li class="item-thumbs span2 design" data-id="id-0" data-type="design">
                    <div class="team-box thumbnail containerx" style="padding:5px 5px 10px;">
                        <img src="<?=base_url()?>asset/img_servis/<?=$key['gbr_servis']?>" style="max-width:100%;" class="image">
                        <div class="middle">
                          <div class="text"><?=$key['jdl_servis']?></div>
                        </div>
                    </div>
                  </li>
                <?php
                  }
                ?>
                <!-- End Item Project -->
              </ul>
            </section>

          </div>
      </div>
    </div>

    <!-- divider -->
    <div class="row">
      <div class="span12">
        <div class="solidline"></div>
      </div>
    </div>
    <!-- end divider -->

  </div>
</section>