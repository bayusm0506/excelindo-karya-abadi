<style type="text/css">
  .mainflip {
      -webkit-transition: 1s;
      -webkit-transform-style: preserve-3d;
      -ms-transition: 1s;
      -moz-transition: 1s;
      -moz-transform: perspective(1000px);
      -moz-transform-style: preserve-3d;
      -ms-transform-style: preserve-3d;
      transition: 1s;
      transform-style: preserve-3d;
      position: relative;
  }

  .frontside {
      position: relative;
      -webkit-transform: rotateY(0deg);
      -ms-transform: rotateY(0deg);
      z-index: 2;
      margin-bottom: 30px;
  }

  .frontside,

  .frontside .card {
      min-height: 312px;
  }

  .frontside .card .card-title {
      color: #007b5e !important;
  }

  .frontside .card .card-body img {
      width: 120px;
      height: 120px;
      border-radius: 50%;
  }
  .separator {
      position: relative;
      content: "";
      left: 50%;
      margin-left: -80px;
      width: 160px;
      border-bottom: 1px solid #e0e0e0;
  }
  .text-uppercase {
      text-transform: uppercase;
  }
</style>
<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="<?=site_url()?>">Home</a> <i class="icon-angle-right"></i></li>
            <li class="active">Our Team</li>
          </ul>
          <h2>Our Team</h2>
        </div>
      </div>
    </div>
  </div>
</section
<section id="content">
  <div class="container">
    <?php
      $cekTeam = $team->num_rows();
      if ($cekTeam > 0) {
    ?>
      <div class="row">
        <?php
          foreach ($team->result_array() as $p) {
        ?>
          <div class="span6">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p><img class="image-holder center-block img-circle" src="<?=base_url()?>asset/img_team/<?=$p['foto']?>" alt="card image"></p>
                                    <h2 class="card-title" style="font-size: 40px;line-height: 50px;"><?=$p['nama']?></h2>
                                    <p class="separator"></p>
                                    <h5 class="text-uppercase"><?=$p['jabatan']?></h5>
                                    <?=$p['deskripsi']?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
          </div>
        <?php } ?>
      </div>
    <?php } ?>
    <?php
      $cekChoose = getChoose()->num_rows();
      if ($cekChoose > 0) {
    ?>
    <div class="row">
      <div class="span12 aligncenter">
        <h3 class="title">WHY CHOOSE <strong>US</strong></h3>
      </div>
    </div>
    <div class="row">
      <?php
        $array_choose = getChoose()->result_array();
        foreach ($array_choose as $p) {
          //$explodeJudul = explode(" ", $p['judul']);
      ?>
      <div class="span4">
        <div class="service-box aligncenter flyLeft">
          <div class="icon">
            <i class="<?=$p['icon']?>"></i>
          </div>
          <h5><span class="colored"> <?=$p['judul']?></span></h5>
          <p>
            <?=$p['keterangan']?>
          </p>

        </div>
      </div>
      <?php } ?>
    </div>
    <?php
      }
    ?>
  </div>
</section>