<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog" style="display:none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Form</h3>
            </div>
            <div class="modal-body form">
                <div class="row">
                    <?php echo form_open_multipart('vacancies/save', array('id'=>'form', 'role'=>'form', 'name'=>'form'));?>
                        <div class="span6">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                 <!--  <th scope="col">#</th>
                                  <th scope="col">First</th>
                                  <th scope="col">Last</th>
                                  <th scope="col">Handle</th> -->
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th scope="row">Nama</th>
                                  <td><input type="text" name="name" style="width: 460px"></td>
                                  <span class="help-block"></span>
                                </tr>
                                <tr>
                                  <th scope="row">Phone</th>
                                  <td><input type="text" name="phone" style="width: 460px"></td>
                                  <span class="help-block"></span>
                                </tr>
                                <tr>
                                  <th scope="row">Email</th>
                                  <td><input type="email" name="email" style="width: 460px"></td>
                                  <span class="help-block"></span>
                                </tr>
                                <tr>
                                  <th scope="row">Position</th>
                                  <td>
                                    <select name="position" style="width: 475px">
                                        <option value="">Choose Position</option>
                                        <?php
                                            foreach ($position as $p) {
                                        ?>
                                            <option value="<?=$p['id_position']?>"><?=$p['position']?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="help-block"></span>
                                  </td>
                                </tr>
                                <tr>
                                  <th scope="row">Attachment</th>
                                  <td>
                                    <input type="file" name="attach1">
                                    <input type="file" name="attach2">
                                    <input type="file" name="attach3">
                                    <input type="file" name="attach4">
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                        <div class="span3">
                            <h3 id="posisi"></h3>
                            <span id="keterangan"></span>
                            <button type="submit" onclick="return confirm('Periksa data Anda dengan baik sebelum menyimpannya');" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button><br />
                            Note :
                            * Please Attach Documents in .zip/.rar file: <br />
                            1. Application Letter <br />
                            2. Photo <br />
                            3. ID Card <br />
                            4. Curiculum Vitae <br />
                            5. Reference Letter <br />
                            6. Certificates <br />
                            7. Others <br />
                        </div>
                    <?php echo form_close();?>
                </div>
            </div>
            <div class="modal-footer">
                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script>
    $('form#form').submit(function(){
        Ajax.upload( "<?=site_url('vacancies/save')?>",'form',function(response){
          if( response.status == 'info'){
            Ajax.show_alert('info',response.msg);
            $('form#form')[0].reset();
            $('#modal_form').modal('hide');
          }else{
            Ajax.show_alert('error',response.msg);
          }
          Ajax.progressFull();
        });
        return false;
      });
</script>
