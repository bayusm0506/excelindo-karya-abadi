<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="<?=site_url()?>">Home</a> <i class="icon-angle-right"></i></li>
            <li class="active">Vacancies</li>
          </ul>
          <h2>Vacancies</h2>
        </div>
      </div>
    </div>
  </div>
</section>
<style type="text/css">
  .blogPost--small {
      color: #7c7b7b;
      margin: 20px 30px 40px 0;
  }
  .blogPost--small .media > .pull-left {
      margin-right: 25px;
  }
  .media > .pull-left {
      margin-right: 29px;
  }
  .blogPost--small a {
      color: #662d91;
  }
  .blogPost--small .date, .blogPost--small .dateEl2 {
      display: block;
      text-align: center;
      padding: 18px 0 25px 0;
      color: #fff;
      /*background: #662d91;*/
      background: #19a1dd;
      width: 113px;
  }
  .blogPost--small .date span, .blogPost--small .dateEl2 span {
      display: inline-block;
      font-size: 42px;
      line-height: 1;
      letter-spacing: 0em;
      text-indent: -0.1em;
      color: #fff;
      font-weight: bold;
  }
  .blogPost--small .date small, .blogPost--small .dateEl2 small {
      display: block;
      font-size: 18px;
      text-transform: uppercase;
      color: #fff;
  }
  .blogPost--small h4 {
      margin: 0 0 12px 0;
      font-size: 22px;
      text-transform: uppercase;
  }
  .media .media-heading a {
      color: #022235;
      font-size: 21px;
      text-transform: uppercase;
  }
</style>
<section id="content">
  <div class="container">
    <div class="row">
      <?php
        $cekData = $list->num_rows();
        if ($cekData > 0) {
          $data = $list->result_array();
          foreach ($data as $p){
      ?>
      <div class="span6">
        <div class="blogPost--small">
          <div class="media">
            <?php
              $tanggal = explode(" ", tgl_indo($p['tgl_deadline']));
            ?>
            <span class="pull-left"><a href="#"><span class="date"><span><?=$tanggal[0]?></span> <small><?=$tanggal[1]?></small></span></a></span>
            <div class="media-body">
              <h4 class="media-heading">
                <a href="#"><?=$p['position']?></a>
              </h4>
              <p>
                Tanggal Deadline : <?=tgl_indo($p['tgl_deadline'])?>
              </p>

              <!-- <a href="#myModal" role="button" class="btn btn-inverse btn-rounded" data-toggle="modal" style="color: white;">Launch demo modal</a> -->
              <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Submit" onclick="vac_submit(<?=$p['id_position']?>)" style="color: white;"><i class="icon-rocket icon-white"></i> Lamar Posisi Ini</a>
            </div>
          </div>
        </div><!-- / blogPost -->
      </div>
      <?php
          }
        }else{
          echo "<div class='span12'><center><h4>Vacancies is not Available</h4></center></div>";
        }
      ?>
    </div>
    <div id="pagination">
      <?php echo $this->pagination->create_links(); ?>
    </div><br />
    <?php
      $cekChoose = getChoose()->num_rows();
      if ($cekChoose > 0) {
    ?>
    <div class="row">
      <div class="span12 aligncenter">
        <?php if(get_cookie('lang_is') === 'en'){ ?>
          <h3 class="title">WHY CHOOSE <strong>US</strong></h3>
        <?php }else{ ?>
          <h3 class="title">Mengapa Memilih <strong>Kami</strong></h3>
        <?php } ?>
      </div>
    </div>
    <div class="row">
      <?php
        $array_choose = getChoose()->result_array();
        foreach ($array_choose as $p) {
          //$explodeJudul = explode(" ", $p['judul']);
      ?>
      <div class="span4">
        <div class="service-box aligncenter flyLeft">
          <div class="icon">
            <i class="<?=$p['icon']?>"></i>
          </div>
          <h5><span class="colored"> <?=$p['judul']?></span></h5>
          <p>
            <?=$p['keterangan']?>
          </p>

        </div>
      </div>
      <?php } ?>
    </div>
    <?php
      }
    ?>
  </div>
</section>

<script type="text/javascript">
  function vac_submit(id)
  {
      save_method = 'submit';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
      
      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo site_url('vacancies/ajax_edit')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('[name="position"]').val(data.id_position);
              $('#keterangan').html(data.keterangan);
              $('#posisi').html(data.position);
              $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
              $('.modal-title').text('Form Vacancies'); // Set title to Bootstrap modal title
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }
</script>

<?php $this->load->view('vacancies/_form'); ?>