<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="inner-heading">
          <ul class="breadcrumb">
            <li><a href="<?=base_url()?>">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="#">Video</a></li>
            <li class="active"></li>
          </ul>
          <h2><?=$title?></h2>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="content" style="background-image: url(); background-repeat: no-repeat; background-size: cover;">
  <div class="container">
    <div class="row">
      <div class="span12">
        <?php
          foreach ($video->result_array() as $p) {
        ?>
        <article>
          <div class="row">
            <?php
              if ($p['id_vid']%2==0) {
            ?>
              <div class="span6">
                <div class="post-video">                
                  <div class="video-container">
                    <iframe src="<?=$p['link']?>" allowfullscreen>
                    </iframe>
                  </div>
                </div>

              </div>
              <div class="span6">
                <div class="post-heading">
                  <h3><a href="#"><?=$p['judul']?></a></h3>
                </div>
                <div class="post-entry">
                  <?=$p['keterangan']?>
                </div>
              </div>
            <?php
              }else{
            ?>
                <div class="span6">
                  <div class="post-heading">
                    <h3><a href="#"><?=$p['judul']?></a></h3>
                  </div>
                  <div class="post-entry">
                    <?=$p['keterangan']?>
                  </div>
                </div>
                <div class="span6">
                  <div class="post-video">                
                    <div class="video-container">
                      <iframe src="<?=$p['link']?>" allowfullscreen>
                      </iframe>
                    </div>
                  </div>
                </div>
            <?php
              }
            ?>
          </div>
        </article>
        <?php
          }
        ?>
      </div>
    </div>

    <!-- divider -->
    <div class="row">
      <div class="span12">
        <div class="solidline"></div>
      </div>
    </div>
    <!-- end divider -->

  </div>
</section>